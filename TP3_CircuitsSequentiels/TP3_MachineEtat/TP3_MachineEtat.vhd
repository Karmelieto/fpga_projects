LIBRARY ieee;
use ieee.std_logic_1164.all;
ENTITY TP3_MachineEtat IS PORT 
(clk, rst, e : IN STD_LOGIC;
		 s : OUT STD_LOGIC);
END TP3_MachineEtat;


Architecture archi of TP3_MachineEtat IS

TYPE Etat is (ON1, ON2, ON3, OFF);
Signal Etat_present : Etat; 
Signal Etat_futur : Etat;

BEGIN

Sequentiel_maj_etat : process (clk, rst)
begin
	if rst = '0' then
		Etat_present <= OFF;
	elsif clk'event and clk = '1' then
		Etat_present <= Etat_futur;

		end if;
end process Sequentiel_maj_etat;


Combinatoire_etats : process (e, Etat_present)
begin
case Etat_present is

	when OFF => if e = '1' then
						Etat_futur <= ON1;
					else
		Etat_futur <= OFF;
					end if;
					
	when ON1 => Etat_futur <= ON2;

	when ON2 => Etat_futur <= ON3;
	
	when ON3 => Etat_futur <= OFF;
	
end case;

end process Combinatoire_etats;


Combinatoire_sorties : process (Etat_present)
begin
	case Etat_present is
		when OFF => s <= '0';
		when ON1 => s <= '1';
		when ON2 => s <= '1';
		when ON3 => s <= '1';
end case;
end process Combinatoire_sorties;
end ;