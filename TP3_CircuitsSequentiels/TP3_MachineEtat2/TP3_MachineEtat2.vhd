LIBRARY ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
ENTITY TP3_MachineEtat2 IS PORT 
(rst, clk, cnt_eq0, e : IN STD_LOGIC;
         cnt_sel, s : OUT STD_LOGIC);
END TP3_MachineEtat2;
 
 
Architecture archi of TP3_MachineEtat2 IS
 
TYPE Etat is (ON1,OFF);
Signal Etat_present : Etat; 
Signal Etat_futur : Etat;
signal cpt : signed(1 downto 0)   := "00";
 
BEGIN
 
Sequentiel_maj_etat : process (clk, rst)
begin
    if rst = '0' then
        Etat_present <= OFF;
    elsif clk'event and clk = '1' then
        Etat_present <= Etat_futur;
 
        end if;
end process Sequentiel_maj_etat;
 
 
Combinatoire_etats : process (e, cnt_eq0, Etat_present)
begin
case Etat_present is
	when OFF => 
		if e = '1' then
		  Etat_futur <= ON1;
      else
        Etat_futur <= OFF;
      end if;
		
    when ON1 =>
		if cnt_eq0 = '1' then
		  Etat_futur <= OFF;
		else
        Etat_futur <= ON1;
	end if;
end case;
 
end process Combinatoire_etats;
 
 
Combinatoire_sorties : process (Etat_present)
begin
case Etat_present is
	when OFF => 
	  S <= '0';
	  cnt_sel <= '0';
	when ON1 =>
	  if clk'event and clk = '1' then
		if cpt = "00" then
			S <= '1';
			cnt_sel <= '1';
		else
		cpt <= cpt-1;
	  end if;
	 end if;
end case;
end process Combinatoire_sorties;
end ;