LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux IS
	PORT (a, b, s: IN STD_LOGIC; y: OUT STD_LOGIC);
END mux;

ARCHITECTURE pure_logic OF mux IS
BEGIN
	y<= (a AND NOT s) OR (b AND s);

END pure_logic;