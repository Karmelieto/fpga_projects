LIBRARY ieee; 
USE ieee.std_logic_1164.all; 


entity MUXCOND is port( 
	E0,E1 : in std_logic; 
	SEL0 : in std_logic; 
	S : out std_logic); 
end MUXCOND; 

architecture CMP1_MUX of MUXCOND is begin 
process(E0,E1,SEL0) 
begin 

if (SEL0='0') then 
	S <= E0; 
elsif (SEL0='1') then 
	S <= E1; 
end if; 

end process; 
end CMP1_MUX; 