LIBRARY ieee;   

USE ieee.std_logic_1164.all;   

  

entity MUX is port(  

E0,E1 : in std_logic;  

SEL : in std_logic; 

S : out std_logic); 

end MUX; 

  

architecture CMP2_MUX of MUX is 

begin 

process(E0,E1,SEL) 

begin 

    case SEL is 

        when '0' => S <= E0; 

        when '1' => S <= E1; 

        when others => S <= null; 

    end case; 

    end process; 

end CMP2_MUX; 