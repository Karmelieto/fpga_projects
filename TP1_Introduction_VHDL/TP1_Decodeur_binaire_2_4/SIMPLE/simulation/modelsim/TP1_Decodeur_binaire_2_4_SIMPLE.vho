-- Copyright (C) 2019  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 19.1.0 Build 670 09/22/2019 SJ Lite Edition"

-- DATE "04/27/2020 14:42:46"

-- 
-- Device: Altera EP4CE115F29I8L Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCEO~	=>  Location: PIN_P28,	 I/O Standard: 2.5 V,	 Current Strength: 8mA


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	decodeur IS
    PORT (
	e_0 : IN std_logic;
	e_1 : IN std_logic;
	s_3 : OUT std_logic;
	s_2 : OUT std_logic;
	s_1 : OUT std_logic;
	s_0 : OUT std_logic
	);
END decodeur;

-- Design Ports Information
-- s_3	=>  Location: PIN_A7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- s_2	=>  Location: PIN_F12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- s_1	=>  Location: PIN_B7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- s_0	=>  Location: PIN_G12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- e_0	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- e_1	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF decodeur IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_e_0 : std_logic;
SIGNAL ww_e_1 : std_logic;
SIGNAL ww_s_3 : std_logic;
SIGNAL ww_s_2 : std_logic;
SIGNAL ww_s_1 : std_logic;
SIGNAL ww_s_0 : std_logic;
SIGNAL \s_3~output_o\ : std_logic;
SIGNAL \s_2~output_o\ : std_logic;
SIGNAL \s_1~output_o\ : std_logic;
SIGNAL \s_0~output_o\ : std_logic;
SIGNAL \e_1~input_o\ : std_logic;
SIGNAL \e_0~input_o\ : std_logic;
SIGNAL \s_1~0_combout\ : std_logic;
SIGNAL \s_1~1_combout\ : std_logic;
SIGNAL \s_1~2_combout\ : std_logic;
SIGNAL \s_0~0_combout\ : std_logic;
SIGNAL \ALT_INV_s_0~0_combout\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_e_0 <= e_0;
ww_e_1 <= e_1;
s_3 <= ww_s_3;
s_2 <= ww_s_2;
s_1 <= ww_s_1;
s_0 <= ww_s_0;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_s_0~0_combout\ <= NOT \s_0~0_combout\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X29_Y73_N2
\s_3~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_1~0_combout\,
	devoe => ww_devoe,
	o => \s_3~output_o\);

-- Location: IOOBUF_X33_Y73_N9
\s_2~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_1~1_combout\,
	devoe => ww_devoe,
	o => \s_2~output_o\);

-- Location: IOOBUF_X29_Y73_N9
\s_1~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \s_1~2_combout\,
	devoe => ww_devoe,
	o => \s_1~output_o\);

-- Location: IOOBUF_X27_Y73_N9
\s_0~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_s_0~0_combout\,
	devoe => ww_devoe,
	o => \s_0~output_o\);

-- Location: IOIBUF_X31_Y73_N1
\e_1~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_e_1,
	o => \e_1~input_o\);

-- Location: IOIBUF_X31_Y73_N8
\e_0~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_e_0,
	o => \e_0~input_o\);

-- Location: LCCOMB_X31_Y72_N0
\s_1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_1~0_combout\ = (\e_1~input_o\ & \e_0~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \e_1~input_o\,
	datad => \e_0~input_o\,
	combout => \s_1~0_combout\);

-- Location: LCCOMB_X31_Y72_N2
\s_1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_1~1_combout\ = (!\e_1~input_o\ & \e_0~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \e_1~input_o\,
	datad => \e_0~input_o\,
	combout => \s_1~1_combout\);

-- Location: LCCOMB_X31_Y72_N4
\s_1~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_1~2_combout\ = (\e_1~input_o\ & !\e_0~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \e_1~input_o\,
	datad => \e_0~input_o\,
	combout => \s_1~2_combout\);

-- Location: LCCOMB_X31_Y72_N6
\s_0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_0~0_combout\ = (\e_1~input_o\) # (\e_0~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \e_1~input_o\,
	datad => \e_0~input_o\,
	combout => \s_0~0_combout\);

ww_s_3 <= \s_3~output_o\;

ww_s_2 <= \s_2~output_o\;

ww_s_1 <= \s_1~output_o\;

ww_s_0 <= \s_0~output_o\;
END structure;


