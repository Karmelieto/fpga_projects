LIBRARY ieee;  

USE ieee.std_logic_1164.all;  

 

ENTITY decodeur IS  

PORT (e_0, e_1: IN STD_LOGIC; 

 s_3, s_2, s_1, s_0: OUT STD_LOGIC);  

END decodeur;  

ARCHITECTURE pure_logic OF decodeur IS  

BEGIN  

s_0<= (NOT e_0 AND NOT e_1);  

s_1<= (NOT e_0 AND e_1);  

s_2<= (e_0 AND NOT e_1);  

s_3<= (e_0 AND e_1);  

END pure_logic; 