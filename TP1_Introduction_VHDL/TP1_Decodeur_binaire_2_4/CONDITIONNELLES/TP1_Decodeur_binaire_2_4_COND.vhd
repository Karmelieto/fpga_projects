library IEEE;  

use IEEE.STD_LOGIC_1164.all;  

    

 ENTITY DECO_BINCOND IS    

port(   

    input : in STD_LOGIC_VECTOR(1 downto 0);  

    output : out STD_LOGIC_VECTOR(3 downto 0)   

);   

END DECO_BINCOND;    

 

ARCHITECTURE behaviour OF DECO_BINCOND IS    

BEGIN PROCESS(input)  

BEGIN  

  

if(input="00") then  

    output <= "1000";  

elsif(input="01") then 

  output <= "0100";  

elsif(input="10") then  

  output <= "0010";  

elsif(input="11") then  

    output <= "0001";  

end if;  

 

end PROCESS;   

END behaviour; 