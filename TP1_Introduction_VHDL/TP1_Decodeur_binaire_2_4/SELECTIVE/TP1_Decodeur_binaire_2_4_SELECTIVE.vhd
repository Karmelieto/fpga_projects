library IEEE; 

use IEEE.STD_LOGIC_1164.all; 

  

entity decodeurSel is 

port( 

    input : in STD_LOGIC_VECTOR(1 downto 0); 

    output : out STD_LOGIC_VECTOR(3 downto 0) 

); 

end decodeurSel; 

  

architecture behaviour of decodeurSel is 

begin 

  

process(input) 

begin 

  case input is 

    when "00" => output <= "1000"; 

    when "01" => output <= "0100"; 

    when "10" => output <= "0010"; 

    when "11" => output <= "0001"; 

  end case; 

end process; 

  

end behaviour; 