LIBRARY ieee; 

USE ieee.std_logic_1164.all; 

 

entity TP1_Multiplexeur21 is port( 

a,b : in std_logic; 

s : in std_logic; 

y : out std_logic); 

end TP1_Multiplexeur21; 

architecture CMP1_MUX of TP1_Multiplexeur21 is begin 

process(a,b,s) 

begin 

if (s='0') then 

y <= a; 

else 

y <= b; 

end if; 

end process; 

end CMP1_MUX;