LIBRARY ieee;   
USE ieee.std_logic_1164.all;   


ENTITY TP1_Multiplexeur41_VHDL IS PORT 

(E0, E1, E2, E3 : IN STD_LOGIC;  
S0,S1 : in STD_LOGIC;
Z: OUT STD_LOGIC);   

END TP1_Multiplexeur41_VHDL;   

ARCHITECTURE pure_logic OF TP1_Multiplexeur41_VHDL IS   
component TP1_Multiplexeur21 
port(a, b : in STD_LOGIC;
s : in STD_LOGIC;
y: out STD_LOGIC);
end component;
signal m1, m2 : std_logic;

BEGIN   
c1 : TP1_Multiplexeur21 port map(E0,E1,S0,m1);
c2 : TP1_Multiplexeur21 port map(E2,E3,S0,m2);
c3 : TP1_Multiplexeur21 port map(m1,m2,S1,Z);
END pure_logic; 