library IEEE;
use ieee.std_logic_1164.all;

entity TP2_Bascules is port (
clk : in std_logic;
d : in std_logic;
q : buffer std_logic);
end TP2_Bascules;

architecture archi of TP2_Bascules IS
signal no_Q : std_logic;

BEGIN
	process (clk)
	BEGIN
		if rising_edge (clk) then
			no_Q <= (not q); 
			 no_Q <= d;
			end if;
		end process;
end archi; 
