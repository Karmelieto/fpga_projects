library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 
entity TP4_ComparateurSimple is
	port (b: in std_logic_vector(2 downto 0);
	equals : out std_logic);
end TP4_ComparateurSimple;
architecture dataflow of TP4_ComparateurSimple is
begin
	equals <= '1' when (b =  "101") else '0';
end dataflow;
