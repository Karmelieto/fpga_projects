-- Copyright (C) 2019  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 19.1.0 Build 670 09/22/2019 SJ Lite Edition"

-- DATE "05/12/2020 16:54:32"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCEO~	=>  Location: PIN_P28,	 I/O Standard: 2.5 V,	 Current Strength: 8mA


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	TP4_CompteurSimple IS
    PORT (
	clk : IN std_logic;
	reset : IN std_logic;
	updown : IN std_logic;
	compt_sortie : OUT std_logic_vector(2 DOWNTO 0)
	);
END TP4_CompteurSimple;

-- Design Ports Information
-- compt_sortie[0]	=>  Location: PIN_K7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- compt_sortie[1]	=>  Location: PIN_J7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- compt_sortie[2]	=>  Location: PIN_M3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- updown	=>  Location: PIN_J5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reset	=>  Location: PIN_J6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF TP4_CompteurSimple IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_updown : std_logic;
SIGNAL ww_compt_sortie : std_logic_vector(2 DOWNTO 0);
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \compt_sortie[0]~output_o\ : std_logic;
SIGNAL \compt_sortie[1]~output_o\ : std_logic;
SIGNAL \compt_sortie[2]~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \updown~input_o\ : std_logic;
SIGNAL \reset~input_o\ : std_logic;
SIGNAL \s_compt_sortie~0_combout\ : std_logic;
SIGNAL \s_compt_sortie~3_combout\ : std_logic;
SIGNAL \s_compt_sortie~4_combout\ : std_logic;
SIGNAL \s_compt_sortie~1_combout\ : std_logic;
SIGNAL \s_compt_sortie~2_combout\ : std_logic;
SIGNAL s_compt_sortie : std_logic_vector(2 DOWNTO 0);

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk <= clk;
ww_reset <= reset;
ww_updown <= updown;
compt_sortie <= ww_compt_sortie;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X0_Y49_N9
\compt_sortie[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_compt_sortie(0),
	devoe => ww_devoe,
	o => \compt_sortie[0]~output_o\);

-- Location: IOOBUF_X0_Y49_N2
\compt_sortie[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_compt_sortie(1),
	devoe => ww_devoe,
	o => \compt_sortie[1]~output_o\);

-- Location: IOOBUF_X0_Y51_N16
\compt_sortie[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_compt_sortie(2),
	devoe => ww_devoe,
	o => \compt_sortie[2]~output_o\);

-- Location: IOIBUF_X0_Y36_N8
\clk~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G2
\clk~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: IOIBUF_X0_Y50_N22
\updown~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_updown,
	o => \updown~input_o\);

-- Location: IOIBUF_X0_Y50_N15
\reset~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reset,
	o => \reset~input_o\);

-- Location: LCCOMB_X1_Y50_N4
\s_compt_sortie~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_compt_sortie~0_combout\ = (\reset~input_o\ & (!\updown~input_o\)) # (!\reset~input_o\ & ((!s_compt_sortie(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \updown~input_o\,
	datac => s_compt_sortie(0),
	datad => \reset~input_o\,
	combout => \s_compt_sortie~0_combout\);

-- Location: FF_X1_Y50_N5
\s_compt_sortie[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \s_compt_sortie~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_compt_sortie(0));

-- Location: LCCOMB_X1_Y50_N0
\s_compt_sortie~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_compt_sortie~3_combout\ = (\updown~input_o\ & (s_compt_sortie(0) & s_compt_sortie(1))) # (!\updown~input_o\ & (!s_compt_sortie(0) & !s_compt_sortie(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \updown~input_o\,
	datac => s_compt_sortie(0),
	datad => s_compt_sortie(1),
	combout => \s_compt_sortie~3_combout\);

-- Location: LCCOMB_X1_Y50_N16
\s_compt_sortie~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_compt_sortie~4_combout\ = (\reset~input_o\ & (!\updown~input_o\)) # (!\reset~input_o\ & ((s_compt_sortie(2) $ (\s_compt_sortie~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100011101110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \updown~input_o\,
	datab => \reset~input_o\,
	datac => s_compt_sortie(2),
	datad => \s_compt_sortie~3_combout\,
	combout => \s_compt_sortie~4_combout\);

-- Location: FF_X1_Y50_N17
\s_compt_sortie[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \s_compt_sortie~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_compt_sortie(2));

-- Location: LCCOMB_X1_Y50_N2
\s_compt_sortie~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_compt_sortie~1_combout\ = (s_compt_sortie(1) & (\updown~input_o\ $ ((s_compt_sortie(0))))) # (!s_compt_sortie(1) & ((\updown~input_o\ & (s_compt_sortie(0))) # (!\updown~input_o\ & (!s_compt_sortie(0) & s_compt_sortie(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => s_compt_sortie(1),
	datab => \updown~input_o\,
	datac => s_compt_sortie(0),
	datad => s_compt_sortie(2),
	combout => \s_compt_sortie~1_combout\);

-- Location: LCCOMB_X1_Y50_N10
\s_compt_sortie~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_compt_sortie~2_combout\ = (!\reset~input_o\ & \s_compt_sortie~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \reset~input_o\,
	datad => \s_compt_sortie~1_combout\,
	combout => \s_compt_sortie~2_combout\);

-- Location: FF_X1_Y50_N11
\s_compt_sortie[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \s_compt_sortie~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_compt_sortie(1));

ww_compt_sortie(0) <= \compt_sortie[0]~output_o\;

ww_compt_sortie(1) <= \compt_sortie[1]~output_o\;

ww_compt_sortie(2) <= \compt_sortie[2]~output_o\;
END structure;


