LIBRARY ieee;    
USE ieee.std_logic_1164.all;    
 
ENTITY TP4_Cascade_CPT_DECO IS PORT  
(clk_C: IN STD_LOGIC; 
output : out STD_LOGIC_VECTOR(5 downto 0));   
END TP4_Cascade_CPT_DECO;    
 
ARCHITECTURE pure_logic OF TP4_Cascade_CPT_DECO IS

component CompteurMod6
	port (clk, reset : IN STD_LOGIC;
	equals : out std_logic);
end component;
 
component Decodeur
 PORT (
      input : in STD_LOGIC_VECTOR(2 downto 0);
		output : out STD_LOGIC_VECTOR(5 downto 0)
);
END component;
 
signal out_compteur : std_logic; 
  
signal out_decodeur : STD_LOGIC_VECTOR(4 DOWNTO 0);

BEGIN

c1 : Compteur port map(clk_C,reset,out_compteur); 
 
c2 : Decodeur port map(out_compteur, out_decodeur);
 
END pure_logic;
