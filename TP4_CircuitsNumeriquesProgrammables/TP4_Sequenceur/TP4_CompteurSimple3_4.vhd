LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;


ENTITY TP4_CompteurSimple3_4 IS
 PORT (
       clk, clk_enable, reset, updown : IN STD_LOGIC;
       compt_sortie : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
		);
END TP4_CompteurSimple3_4;

ARCHITECTURE rtl OF TP4_CompteurSimple3_4 IS 
	SIGNAL s_compt_sortie : STD_LOGIC_VECTOR(2 DOWNTO 0);
BEGIN
PROCESS(clk)
BEGIN
IF clk'EVENT AND clk = '1' THEN
 if updown = '1' then
	IF reset = '1' THEN 
		s_compt_sortie <= "000";
	ELSE 	
		IF s_compt_sortie >= 7 THEN
				s_compt_sortie <= "000";
		ELSE
		    IF clk_enable = '1' then
				s_compt_sortie <= s_compt_sortie + 1;
			 END IF;	
		END IF;
	END IF;
  else
    IF reset = '1' THEN 
		s_compt_sortie <= "101";
	  ELSE 	
		  IF s_compt_sortie <= "000" THEN
			  	  s_compt_sortie <= "101";
		  ELSE
		      IF clk_enable = '1' then
				  s_compt_sortie <= s_compt_sortie - 1;
				END IF;
		  END IF;
	  END IF;
	end if;  
ELSE s_compt_sortie <= s_compt_sortie;
END IF;
END PROCESS;
	compt_sortie <= s_compt_sortie;
END;
