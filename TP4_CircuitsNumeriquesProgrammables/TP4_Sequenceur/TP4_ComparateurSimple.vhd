library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 
entity TP4_ComparateurSimple is
	port (b: in std_logic_vector(2 downto 0);
	sens : in std_logic;
	equals : out std_logic);
end TP4_ComparateurSimple;

architecture dataflow of TP4_ComparateurSimple is
begin
process(b,sens)
begin

if 

	sens = '1' then
	IF b = "110" then
	  equals <= '1';  
	ELSE 
	  equals <= '0';
	END IF;
	
else

	IF b = "111" then
	  equals <= '1';  
	ELSE 
	  equals <='0';
	END IF;
	
end if;
end process;
end dataflow; 
