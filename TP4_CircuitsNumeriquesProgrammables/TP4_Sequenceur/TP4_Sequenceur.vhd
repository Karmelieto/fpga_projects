LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;


ENTITY TP4_FDiviseurFrequence IS
 PORT (
       clk, clk_enable : IN STD_LOGIC;
       compt_sortie : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
		);
END TP4_FDiviseurFrequence;

ARCHITECTURE rtl OF TP4_FDiviseurFrequence IS 
	SIGNAL s_compt_sortie : STD_LOGIC_VECTOR(2 DOWNTO 0);
BEGIN
PROCESS(clk)
BEGIN
 IF clk'EVENT AND clk = '1' THEN
   IF s_compt_sortie >= 7 THEN
         s_compt_sortie <= "000";
   ELSE
         s_compt_sortie <= s_compt_sortie + 1;
   END IF;
 END IF;
END PROCESS;
	compt_sortie <= s_compt_sortie;
END;