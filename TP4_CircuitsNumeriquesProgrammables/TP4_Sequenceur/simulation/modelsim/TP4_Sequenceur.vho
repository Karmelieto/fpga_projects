-- Copyright (C) 2019  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 19.1.0 Build 670 09/22/2019 SJ Standard Edition"

-- DATE "05/18/2020 21:32:02"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCEO~	=>  Location: PIN_P28,	 I/O Standard: 2.5 V,	 Current Strength: 8mA


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	TP4_SequenceurAntiRebond IS
    PORT (
	BasculeOut : OUT std_logic;
	Clock50MHz : IN std_logic;
	ClockBascule : IN std_logic;
	OutCompteur : OUT std_logic_vector(2 DOWNTO 0);
	sortie : OUT std_logic_vector(5 DOWNTO 0)
	);
END TP4_SequenceurAntiRebond;

-- Design Ports Information
-- BasculeOut	=>  Location: PIN_M28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OutCompteur[2]	=>  Location: PIN_L23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OutCompteur[1]	=>  Location: PIN_K27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OutCompteur[0]	=>  Location: PIN_K28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sortie[5]	=>  Location: PIN_J22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sortie[4]	=>  Location: PIN_L25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sortie[3]	=>  Location: PIN_L26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sortie[2]	=>  Location: PIN_E17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sortie[1]	=>  Location: PIN_F22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- sortie[0]	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ClockBascule	=>  Location: PIN_M23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Clock50MHz	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF TP4_SequenceurAntiRebond IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_BasculeOut : std_logic;
SIGNAL ww_Clock50MHz : std_logic;
SIGNAL ww_ClockBascule : std_logic;
SIGNAL ww_OutCompteur : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_sortie : std_logic_vector(5 DOWNTO 0);
SIGNAL \Clock50MHz~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst4|clk6hz~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \BasculeOut~output_o\ : std_logic;
SIGNAL \OutCompteur[2]~output_o\ : std_logic;
SIGNAL \OutCompteur[1]~output_o\ : std_logic;
SIGNAL \OutCompteur[0]~output_o\ : std_logic;
SIGNAL \sortie[5]~output_o\ : std_logic;
SIGNAL \sortie[4]~output_o\ : std_logic;
SIGNAL \sortie[3]~output_o\ : std_logic;
SIGNAL \sortie[2]~output_o\ : std_logic;
SIGNAL \sortie[1]~output_o\ : std_logic;
SIGNAL \sortie[0]~output_o\ : std_logic;
SIGNAL \Clock50MHz~input_o\ : std_logic;
SIGNAL \Clock50MHz~inputclkctrl_outclk\ : std_logic;
SIGNAL \inst4|clk_sortie[0]~24_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[0]~25\ : std_logic;
SIGNAL \inst4|clk_sortie[1]~26_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[1]~27\ : std_logic;
SIGNAL \inst4|clk_sortie[2]~28_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[2]~29\ : std_logic;
SIGNAL \inst4|clk_sortie[3]~30_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[3]~31\ : std_logic;
SIGNAL \inst4|clk_sortie[4]~32_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[4]~33\ : std_logic;
SIGNAL \inst4|clk_sortie[5]~34_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[5]~35\ : std_logic;
SIGNAL \inst4|clk_sortie[6]~36_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[6]~37\ : std_logic;
SIGNAL \inst4|clk_sortie[7]~38_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[7]~39\ : std_logic;
SIGNAL \inst4|clk_sortie[8]~40_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[8]~41\ : std_logic;
SIGNAL \inst4|clk_sortie[9]~42_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[9]~43\ : std_logic;
SIGNAL \inst4|clk_sortie[10]~44_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[10]~45\ : std_logic;
SIGNAL \inst4|clk_sortie[11]~46_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[11]~47\ : std_logic;
SIGNAL \inst4|clk_sortie[12]~48_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[12]~49\ : std_logic;
SIGNAL \inst4|clk_sortie[13]~50_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[13]~51\ : std_logic;
SIGNAL \inst4|clk_sortie[14]~52_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[14]~53\ : std_logic;
SIGNAL \inst4|clk_sortie[15]~54_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[15]~55\ : std_logic;
SIGNAL \inst4|clk_sortie[16]~56_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[16]~57\ : std_logic;
SIGNAL \inst4|clk_sortie[17]~58_combout\ : std_logic;
SIGNAL \inst4|LessThan0~3_combout\ : std_logic;
SIGNAL \inst4|LessThan0~2_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[17]~59\ : std_logic;
SIGNAL \inst4|clk_sortie[18]~60_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[18]~61\ : std_logic;
SIGNAL \inst4|clk_sortie[19]~62_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[19]~63\ : std_logic;
SIGNAL \inst4|clk_sortie[20]~64_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[20]~65\ : std_logic;
SIGNAL \inst4|clk_sortie[21]~66_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[21]~67\ : std_logic;
SIGNAL \inst4|clk_sortie[22]~68_combout\ : std_logic;
SIGNAL \inst4|clk_sortie[22]~69\ : std_logic;
SIGNAL \inst4|clk_sortie[23]~70_combout\ : std_logic;
SIGNAL \inst4|LessThan0~4_combout\ : std_logic;
SIGNAL \inst4|LessThan0~5_combout\ : std_logic;
SIGNAL \inst4|LessThan0~0_combout\ : std_logic;
SIGNAL \inst4|LessThan0~1_combout\ : std_logic;
SIGNAL \inst4|LessThan0~6_combout\ : std_logic;
SIGNAL \inst4|clk6hz~q\ : std_logic;
SIGNAL \inst4|clk6hz~clkctrl_outclk\ : std_logic;
SIGNAL \ClockBascule~input_o\ : std_logic;
SIGNAL \inst|Selector1~0_combout\ : std_logic;
SIGNAL \inst|Etat_present.E1~feeder_combout\ : std_logic;
SIGNAL \inst|Etat_present.E1~q\ : std_logic;
SIGNAL \inst|Selector2~0_combout\ : std_logic;
SIGNAL \inst|Etat_present.E2~feeder_combout\ : std_logic;
SIGNAL \inst|Etat_present.E2~q\ : std_logic;
SIGNAL \inst|Selector3~0_combout\ : std_logic;
SIGNAL \inst|Etat_present.E3~feeder_combout\ : std_logic;
SIGNAL \inst|Etat_present.E3~q\ : std_logic;
SIGNAL \inst|Selector0~0_combout\ : std_logic;
SIGNAL \inst|Etat_present.E0~feeder_combout\ : std_logic;
SIGNAL \inst|Etat_present.E0~q\ : std_logic;
SIGNAL \inst|updown~0_combout\ : std_logic;
SIGNAL \inst3|LessThan0~0_combout\ : std_logic;
SIGNAL \inst3|s_compt_sortie[2]~12_combout\ : std_logic;
SIGNAL \inst3|s_compt_sortie[0]~16_combout\ : std_logic;
SIGNAL \inst1|equals~0_combout\ : std_logic;
SIGNAL \inst3|s_compt_sortie[0]~17_combout\ : std_logic;
SIGNAL \inst3|s_compt_sortie[0]~18_combout\ : std_logic;
SIGNAL \inst3|LessThan1~0_combout\ : std_logic;
SIGNAL \inst3|s_compt_sortie~13_combout\ : std_logic;
SIGNAL \inst3|s_compt_sortie[1]~14_combout\ : std_logic;
SIGNAL \inst3|s_compt_sortie[1]~15_combout\ : std_logic;
SIGNAL \inst3|s_compt_sortie[2]~8_combout\ : std_logic;
SIGNAL \inst3|s_compt_sortie[2]~19_combout\ : std_logic;
SIGNAL \inst3|s_compt_sortie[2]~20_combout\ : std_logic;
SIGNAL \inst3|s_compt_sortie[2]~9_combout\ : std_logic;
SIGNAL \inst3|s_compt_sortie[2]~11_combout\ : std_logic;
SIGNAL \inst2|output[5]~0_combout\ : std_logic;
SIGNAL \inst2|output[5]~1_combout\ : std_logic;
SIGNAL \inst2|comb~0_combout\ : std_logic;
SIGNAL \inst2|comb~2_combout\ : std_logic;
SIGNAL \inst2|comb~1_combout\ : std_logic;
SIGNAL \inst2|comb~4_combout\ : std_logic;
SIGNAL \inst2|comb~3_combout\ : std_logic;
SIGNAL \inst2|comb~5_combout\ : std_logic;
SIGNAL \inst2|comb~6_combout\ : std_logic;
SIGNAL \inst2|comb~8_combout\ : std_logic;
SIGNAL \inst2|comb~7_combout\ : std_logic;
SIGNAL \inst4|clk_sortie\ : std_logic_vector(23 DOWNTO 0);
SIGNAL \inst3|s_compt_sortie\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \inst2|output\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \inst|ALT_INV_updown~0_combout\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

BasculeOut <= ww_BasculeOut;
ww_Clock50MHz <= Clock50MHz;
ww_ClockBascule <= ClockBascule;
OutCompteur <= ww_OutCompteur;
sortie <= ww_sortie;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\Clock50MHz~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \Clock50MHz~input_o\);

\inst4|clk6hz~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst4|clk6hz~q\);
\inst|ALT_INV_updown~0_combout\ <= NOT \inst|updown~0_combout\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X115_Y45_N16
\BasculeOut~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|ALT_INV_updown~0_combout\,
	devoe => ww_devoe,
	o => \BasculeOut~output_o\);

-- Location: IOOBUF_X115_Y49_N9
\OutCompteur[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|s_compt_sortie\(2),
	devoe => ww_devoe,
	o => \OutCompteur[2]~output_o\);

-- Location: IOOBUF_X115_Y50_N9
\OutCompteur[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|s_compt_sortie\(1),
	devoe => ww_devoe,
	o => \OutCompteur[1]~output_o\);

-- Location: IOOBUF_X115_Y49_N2
\OutCompteur[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|s_compt_sortie\(0),
	devoe => ww_devoe,
	o => \OutCompteur[0]~output_o\);

-- Location: IOOBUF_X115_Y67_N16
\sortie[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|output\(5),
	devoe => ww_devoe,
	o => \sortie[5]~output_o\);

-- Location: IOOBUF_X115_Y54_N16
\sortie[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|output\(4),
	devoe => ww_devoe,
	o => \sortie[4]~output_o\);

-- Location: IOOBUF_X115_Y50_N2
\sortie[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|output\(3),
	devoe => ww_devoe,
	o => \sortie[3]~output_o\);

-- Location: IOOBUF_X67_Y73_N23
\sortie[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|output\(2),
	devoe => ww_devoe,
	o => \sortie[2]~output_o\);

-- Location: IOOBUF_X107_Y73_N23
\sortie[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|output\(1),
	devoe => ww_devoe,
	o => \sortie[1]~output_o\);

-- Location: IOOBUF_X69_Y73_N23
\sortie[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|output\(0),
	devoe => ww_devoe,
	o => \sortie[0]~output_o\);

-- Location: IOIBUF_X0_Y36_N15
\Clock50MHz~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Clock50MHz,
	o => \Clock50MHz~input_o\);

-- Location: CLKCTRL_G4
\Clock50MHz~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \Clock50MHz~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \Clock50MHz~inputclkctrl_outclk\);

-- Location: LCCOMB_X114_Y43_N8
\inst4|clk_sortie[0]~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[0]~24_combout\ = \inst4|clk_sortie\(0) $ (VCC)
-- \inst4|clk_sortie[0]~25\ = CARRY(\inst4|clk_sortie\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(0),
	datad => VCC,
	combout => \inst4|clk_sortie[0]~24_combout\,
	cout => \inst4|clk_sortie[0]~25\);

-- Location: FF_X114_Y43_N9
\inst4|clk_sortie[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[0]~24_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(0));

-- Location: LCCOMB_X114_Y43_N10
\inst4|clk_sortie[1]~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[1]~26_combout\ = (\inst4|clk_sortie\(1) & (!\inst4|clk_sortie[0]~25\)) # (!\inst4|clk_sortie\(1) & ((\inst4|clk_sortie[0]~25\) # (GND)))
-- \inst4|clk_sortie[1]~27\ = CARRY((!\inst4|clk_sortie[0]~25\) # (!\inst4|clk_sortie\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk_sortie\(1),
	datad => VCC,
	cin => \inst4|clk_sortie[0]~25\,
	combout => \inst4|clk_sortie[1]~26_combout\,
	cout => \inst4|clk_sortie[1]~27\);

-- Location: FF_X114_Y43_N11
\inst4|clk_sortie[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[1]~26_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(1));

-- Location: LCCOMB_X114_Y43_N12
\inst4|clk_sortie[2]~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[2]~28_combout\ = (\inst4|clk_sortie\(2) & (\inst4|clk_sortie[1]~27\ $ (GND))) # (!\inst4|clk_sortie\(2) & (!\inst4|clk_sortie[1]~27\ & VCC))
-- \inst4|clk_sortie[2]~29\ = CARRY((\inst4|clk_sortie\(2) & !\inst4|clk_sortie[1]~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk_sortie\(2),
	datad => VCC,
	cin => \inst4|clk_sortie[1]~27\,
	combout => \inst4|clk_sortie[2]~28_combout\,
	cout => \inst4|clk_sortie[2]~29\);

-- Location: FF_X114_Y43_N13
\inst4|clk_sortie[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[2]~28_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(2));

-- Location: LCCOMB_X114_Y43_N14
\inst4|clk_sortie[3]~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[3]~30_combout\ = (\inst4|clk_sortie\(3) & (!\inst4|clk_sortie[2]~29\)) # (!\inst4|clk_sortie\(3) & ((\inst4|clk_sortie[2]~29\) # (GND)))
-- \inst4|clk_sortie[3]~31\ = CARRY((!\inst4|clk_sortie[2]~29\) # (!\inst4|clk_sortie\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(3),
	datad => VCC,
	cin => \inst4|clk_sortie[2]~29\,
	combout => \inst4|clk_sortie[3]~30_combout\,
	cout => \inst4|clk_sortie[3]~31\);

-- Location: FF_X114_Y43_N15
\inst4|clk_sortie[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[3]~30_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(3));

-- Location: LCCOMB_X114_Y43_N16
\inst4|clk_sortie[4]~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[4]~32_combout\ = (\inst4|clk_sortie\(4) & (\inst4|clk_sortie[3]~31\ $ (GND))) # (!\inst4|clk_sortie\(4) & (!\inst4|clk_sortie[3]~31\ & VCC))
-- \inst4|clk_sortie[4]~33\ = CARRY((\inst4|clk_sortie\(4) & !\inst4|clk_sortie[3]~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(4),
	datad => VCC,
	cin => \inst4|clk_sortie[3]~31\,
	combout => \inst4|clk_sortie[4]~32_combout\,
	cout => \inst4|clk_sortie[4]~33\);

-- Location: FF_X114_Y43_N17
\inst4|clk_sortie[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[4]~32_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(4));

-- Location: LCCOMB_X114_Y43_N18
\inst4|clk_sortie[5]~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[5]~34_combout\ = (\inst4|clk_sortie\(5) & (!\inst4|clk_sortie[4]~33\)) # (!\inst4|clk_sortie\(5) & ((\inst4|clk_sortie[4]~33\) # (GND)))
-- \inst4|clk_sortie[5]~35\ = CARRY((!\inst4|clk_sortie[4]~33\) # (!\inst4|clk_sortie\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(5),
	datad => VCC,
	cin => \inst4|clk_sortie[4]~33\,
	combout => \inst4|clk_sortie[5]~34_combout\,
	cout => \inst4|clk_sortie[5]~35\);

-- Location: FF_X114_Y43_N19
\inst4|clk_sortie[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[5]~34_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(5));

-- Location: LCCOMB_X114_Y43_N20
\inst4|clk_sortie[6]~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[6]~36_combout\ = (\inst4|clk_sortie\(6) & (\inst4|clk_sortie[5]~35\ $ (GND))) # (!\inst4|clk_sortie\(6) & (!\inst4|clk_sortie[5]~35\ & VCC))
-- \inst4|clk_sortie[6]~37\ = CARRY((\inst4|clk_sortie\(6) & !\inst4|clk_sortie[5]~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(6),
	datad => VCC,
	cin => \inst4|clk_sortie[5]~35\,
	combout => \inst4|clk_sortie[6]~36_combout\,
	cout => \inst4|clk_sortie[6]~37\);

-- Location: FF_X114_Y43_N21
\inst4|clk_sortie[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[6]~36_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(6));

-- Location: LCCOMB_X114_Y43_N22
\inst4|clk_sortie[7]~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[7]~38_combout\ = (\inst4|clk_sortie\(7) & (!\inst4|clk_sortie[6]~37\)) # (!\inst4|clk_sortie\(7) & ((\inst4|clk_sortie[6]~37\) # (GND)))
-- \inst4|clk_sortie[7]~39\ = CARRY((!\inst4|clk_sortie[6]~37\) # (!\inst4|clk_sortie\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk_sortie\(7),
	datad => VCC,
	cin => \inst4|clk_sortie[6]~37\,
	combout => \inst4|clk_sortie[7]~38_combout\,
	cout => \inst4|clk_sortie[7]~39\);

-- Location: FF_X114_Y43_N23
\inst4|clk_sortie[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[7]~38_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(7));

-- Location: LCCOMB_X114_Y43_N24
\inst4|clk_sortie[8]~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[8]~40_combout\ = (\inst4|clk_sortie\(8) & (\inst4|clk_sortie[7]~39\ $ (GND))) # (!\inst4|clk_sortie\(8) & (!\inst4|clk_sortie[7]~39\ & VCC))
-- \inst4|clk_sortie[8]~41\ = CARRY((\inst4|clk_sortie\(8) & !\inst4|clk_sortie[7]~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(8),
	datad => VCC,
	cin => \inst4|clk_sortie[7]~39\,
	combout => \inst4|clk_sortie[8]~40_combout\,
	cout => \inst4|clk_sortie[8]~41\);

-- Location: FF_X114_Y43_N25
\inst4|clk_sortie[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[8]~40_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(8));

-- Location: LCCOMB_X114_Y43_N26
\inst4|clk_sortie[9]~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[9]~42_combout\ = (\inst4|clk_sortie\(9) & (!\inst4|clk_sortie[8]~41\)) # (!\inst4|clk_sortie\(9) & ((\inst4|clk_sortie[8]~41\) # (GND)))
-- \inst4|clk_sortie[9]~43\ = CARRY((!\inst4|clk_sortie[8]~41\) # (!\inst4|clk_sortie\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk_sortie\(9),
	datad => VCC,
	cin => \inst4|clk_sortie[8]~41\,
	combout => \inst4|clk_sortie[9]~42_combout\,
	cout => \inst4|clk_sortie[9]~43\);

-- Location: FF_X114_Y43_N27
\inst4|clk_sortie[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[9]~42_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(9));

-- Location: LCCOMB_X114_Y43_N28
\inst4|clk_sortie[10]~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[10]~44_combout\ = (\inst4|clk_sortie\(10) & (\inst4|clk_sortie[9]~43\ $ (GND))) # (!\inst4|clk_sortie\(10) & (!\inst4|clk_sortie[9]~43\ & VCC))
-- \inst4|clk_sortie[10]~45\ = CARRY((\inst4|clk_sortie\(10) & !\inst4|clk_sortie[9]~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(10),
	datad => VCC,
	cin => \inst4|clk_sortie[9]~43\,
	combout => \inst4|clk_sortie[10]~44_combout\,
	cout => \inst4|clk_sortie[10]~45\);

-- Location: FF_X114_Y43_N29
\inst4|clk_sortie[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[10]~44_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(10));

-- Location: LCCOMB_X114_Y43_N30
\inst4|clk_sortie[11]~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[11]~46_combout\ = (\inst4|clk_sortie\(11) & (!\inst4|clk_sortie[10]~45\)) # (!\inst4|clk_sortie\(11) & ((\inst4|clk_sortie[10]~45\) # (GND)))
-- \inst4|clk_sortie[11]~47\ = CARRY((!\inst4|clk_sortie[10]~45\) # (!\inst4|clk_sortie\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk_sortie\(11),
	datad => VCC,
	cin => \inst4|clk_sortie[10]~45\,
	combout => \inst4|clk_sortie[11]~46_combout\,
	cout => \inst4|clk_sortie[11]~47\);

-- Location: FF_X114_Y43_N31
\inst4|clk_sortie[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[11]~46_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(11));

-- Location: LCCOMB_X114_Y42_N0
\inst4|clk_sortie[12]~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[12]~48_combout\ = (\inst4|clk_sortie\(12) & (\inst4|clk_sortie[11]~47\ $ (GND))) # (!\inst4|clk_sortie\(12) & (!\inst4|clk_sortie[11]~47\ & VCC))
-- \inst4|clk_sortie[12]~49\ = CARRY((\inst4|clk_sortie\(12) & !\inst4|clk_sortie[11]~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(12),
	datad => VCC,
	cin => \inst4|clk_sortie[11]~47\,
	combout => \inst4|clk_sortie[12]~48_combout\,
	cout => \inst4|clk_sortie[12]~49\);

-- Location: FF_X114_Y42_N1
\inst4|clk_sortie[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[12]~48_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(12));

-- Location: LCCOMB_X114_Y42_N2
\inst4|clk_sortie[13]~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[13]~50_combout\ = (\inst4|clk_sortie\(13) & (!\inst4|clk_sortie[12]~49\)) # (!\inst4|clk_sortie\(13) & ((\inst4|clk_sortie[12]~49\) # (GND)))
-- \inst4|clk_sortie[13]~51\ = CARRY((!\inst4|clk_sortie[12]~49\) # (!\inst4|clk_sortie\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(13),
	datad => VCC,
	cin => \inst4|clk_sortie[12]~49\,
	combout => \inst4|clk_sortie[13]~50_combout\,
	cout => \inst4|clk_sortie[13]~51\);

-- Location: FF_X114_Y42_N3
\inst4|clk_sortie[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[13]~50_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(13));

-- Location: LCCOMB_X114_Y42_N4
\inst4|clk_sortie[14]~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[14]~52_combout\ = (\inst4|clk_sortie\(14) & (\inst4|clk_sortie[13]~51\ $ (GND))) # (!\inst4|clk_sortie\(14) & (!\inst4|clk_sortie[13]~51\ & VCC))
-- \inst4|clk_sortie[14]~53\ = CARRY((\inst4|clk_sortie\(14) & !\inst4|clk_sortie[13]~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(14),
	datad => VCC,
	cin => \inst4|clk_sortie[13]~51\,
	combout => \inst4|clk_sortie[14]~52_combout\,
	cout => \inst4|clk_sortie[14]~53\);

-- Location: FF_X114_Y42_N5
\inst4|clk_sortie[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[14]~52_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(14));

-- Location: LCCOMB_X114_Y42_N6
\inst4|clk_sortie[15]~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[15]~54_combout\ = (\inst4|clk_sortie\(15) & (!\inst4|clk_sortie[14]~53\)) # (!\inst4|clk_sortie\(15) & ((\inst4|clk_sortie[14]~53\) # (GND)))
-- \inst4|clk_sortie[15]~55\ = CARRY((!\inst4|clk_sortie[14]~53\) # (!\inst4|clk_sortie\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk_sortie\(15),
	datad => VCC,
	cin => \inst4|clk_sortie[14]~53\,
	combout => \inst4|clk_sortie[15]~54_combout\,
	cout => \inst4|clk_sortie[15]~55\);

-- Location: FF_X114_Y42_N7
\inst4|clk_sortie[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[15]~54_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(15));

-- Location: LCCOMB_X114_Y42_N8
\inst4|clk_sortie[16]~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[16]~56_combout\ = (\inst4|clk_sortie\(16) & (\inst4|clk_sortie[15]~55\ $ (GND))) # (!\inst4|clk_sortie\(16) & (!\inst4|clk_sortie[15]~55\ & VCC))
-- \inst4|clk_sortie[16]~57\ = CARRY((\inst4|clk_sortie\(16) & !\inst4|clk_sortie[15]~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(16),
	datad => VCC,
	cin => \inst4|clk_sortie[15]~55\,
	combout => \inst4|clk_sortie[16]~56_combout\,
	cout => \inst4|clk_sortie[16]~57\);

-- Location: FF_X114_Y42_N9
\inst4|clk_sortie[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[16]~56_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(16));

-- Location: LCCOMB_X114_Y42_N10
\inst4|clk_sortie[17]~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[17]~58_combout\ = (\inst4|clk_sortie\(17) & (!\inst4|clk_sortie[16]~57\)) # (!\inst4|clk_sortie\(17) & ((\inst4|clk_sortie[16]~57\) # (GND)))
-- \inst4|clk_sortie[17]~59\ = CARRY((!\inst4|clk_sortie[16]~57\) # (!\inst4|clk_sortie\(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(17),
	datad => VCC,
	cin => \inst4|clk_sortie[16]~57\,
	combout => \inst4|clk_sortie[17]~58_combout\,
	cout => \inst4|clk_sortie[17]~59\);

-- Location: FF_X114_Y42_N11
\inst4|clk_sortie[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[17]~58_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(17));

-- Location: LCCOMB_X113_Y42_N16
\inst4|LessThan0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|LessThan0~3_combout\ = (!\inst4|clk_sortie\(15) & (!\inst4|clk_sortie\(17) & (!\inst4|clk_sortie\(16) & !\inst4|clk_sortie\(14))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk_sortie\(15),
	datab => \inst4|clk_sortie\(17),
	datac => \inst4|clk_sortie\(16),
	datad => \inst4|clk_sortie\(14),
	combout => \inst4|LessThan0~3_combout\);

-- Location: LCCOMB_X114_Y42_N28
\inst4|LessThan0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|LessThan0~2_combout\ = (!\inst4|clk_sortie\(11) & (!\inst4|clk_sortie\(12) & (!\inst4|clk_sortie\(10) & !\inst4|clk_sortie\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk_sortie\(11),
	datab => \inst4|clk_sortie\(12),
	datac => \inst4|clk_sortie\(10),
	datad => \inst4|clk_sortie\(13),
	combout => \inst4|LessThan0~2_combout\);

-- Location: LCCOMB_X114_Y42_N12
\inst4|clk_sortie[18]~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[18]~60_combout\ = (\inst4|clk_sortie\(18) & (\inst4|clk_sortie[17]~59\ $ (GND))) # (!\inst4|clk_sortie\(18) & (!\inst4|clk_sortie[17]~59\ & VCC))
-- \inst4|clk_sortie[18]~61\ = CARRY((\inst4|clk_sortie\(18) & !\inst4|clk_sortie[17]~59\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk_sortie\(18),
	datad => VCC,
	cin => \inst4|clk_sortie[17]~59\,
	combout => \inst4|clk_sortie[18]~60_combout\,
	cout => \inst4|clk_sortie[18]~61\);

-- Location: FF_X114_Y42_N13
\inst4|clk_sortie[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[18]~60_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(18));

-- Location: LCCOMB_X114_Y42_N14
\inst4|clk_sortie[19]~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[19]~62_combout\ = (\inst4|clk_sortie\(19) & (!\inst4|clk_sortie[18]~61\)) # (!\inst4|clk_sortie\(19) & ((\inst4|clk_sortie[18]~61\) # (GND)))
-- \inst4|clk_sortie[19]~63\ = CARRY((!\inst4|clk_sortie[18]~61\) # (!\inst4|clk_sortie\(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(19),
	datad => VCC,
	cin => \inst4|clk_sortie[18]~61\,
	combout => \inst4|clk_sortie[19]~62_combout\,
	cout => \inst4|clk_sortie[19]~63\);

-- Location: FF_X114_Y42_N15
\inst4|clk_sortie[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[19]~62_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(19));

-- Location: LCCOMB_X114_Y42_N16
\inst4|clk_sortie[20]~64\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[20]~64_combout\ = (\inst4|clk_sortie\(20) & (\inst4|clk_sortie[19]~63\ $ (GND))) # (!\inst4|clk_sortie\(20) & (!\inst4|clk_sortie[19]~63\ & VCC))
-- \inst4|clk_sortie[20]~65\ = CARRY((\inst4|clk_sortie\(20) & !\inst4|clk_sortie[19]~63\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(20),
	datad => VCC,
	cin => \inst4|clk_sortie[19]~63\,
	combout => \inst4|clk_sortie[20]~64_combout\,
	cout => \inst4|clk_sortie[20]~65\);

-- Location: FF_X114_Y42_N17
\inst4|clk_sortie[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[20]~64_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(20));

-- Location: LCCOMB_X114_Y42_N18
\inst4|clk_sortie[21]~66\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[21]~66_combout\ = (\inst4|clk_sortie\(21) & (!\inst4|clk_sortie[20]~65\)) # (!\inst4|clk_sortie\(21) & ((\inst4|clk_sortie[20]~65\) # (GND)))
-- \inst4|clk_sortie[21]~67\ = CARRY((!\inst4|clk_sortie[20]~65\) # (!\inst4|clk_sortie\(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(21),
	datad => VCC,
	cin => \inst4|clk_sortie[20]~65\,
	combout => \inst4|clk_sortie[21]~66_combout\,
	cout => \inst4|clk_sortie[21]~67\);

-- Location: FF_X114_Y42_N19
\inst4|clk_sortie[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[21]~66_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(21));

-- Location: LCCOMB_X114_Y42_N20
\inst4|clk_sortie[22]~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[22]~68_combout\ = (\inst4|clk_sortie\(22) & (\inst4|clk_sortie[21]~67\ $ (GND))) # (!\inst4|clk_sortie\(22) & (!\inst4|clk_sortie[21]~67\ & VCC))
-- \inst4|clk_sortie[22]~69\ = CARRY((\inst4|clk_sortie\(22) & !\inst4|clk_sortie[21]~67\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(22),
	datad => VCC,
	cin => \inst4|clk_sortie[21]~67\,
	combout => \inst4|clk_sortie[22]~68_combout\,
	cout => \inst4|clk_sortie[22]~69\);

-- Location: FF_X114_Y42_N21
\inst4|clk_sortie[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[22]~68_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(22));

-- Location: LCCOMB_X114_Y42_N22
\inst4|clk_sortie[23]~70\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|clk_sortie[23]~70_combout\ = \inst4|clk_sortie\(23) $ (\inst4|clk_sortie[22]~69\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk_sortie\(23),
	cin => \inst4|clk_sortie[22]~69\,
	combout => \inst4|clk_sortie[23]~70_combout\);

-- Location: FF_X114_Y42_N23
\inst4|clk_sortie[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|clk_sortie[23]~70_combout\,
	sclr => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk_sortie\(23));

-- Location: LCCOMB_X114_Y42_N24
\inst4|LessThan0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|LessThan0~4_combout\ = (!\inst4|clk_sortie\(18) & (!\inst4|clk_sortie\(20) & (!\inst4|clk_sortie\(19) & !\inst4|clk_sortie\(21))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk_sortie\(18),
	datab => \inst4|clk_sortie\(20),
	datac => \inst4|clk_sortie\(19),
	datad => \inst4|clk_sortie\(21),
	combout => \inst4|LessThan0~4_combout\);

-- Location: LCCOMB_X114_Y42_N26
\inst4|LessThan0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|LessThan0~5_combout\ = (!\inst4|clk_sortie\(22) & (!\inst4|clk_sortie\(23) & \inst4|LessThan0~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst4|clk_sortie\(22),
	datac => \inst4|clk_sortie\(23),
	datad => \inst4|LessThan0~4_combout\,
	combout => \inst4|LessThan0~5_combout\);

-- Location: LCCOMB_X114_Y43_N0
\inst4|LessThan0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|LessThan0~0_combout\ = (((!\inst4|clk_sortie\(4) & !\inst4|clk_sortie\(3))) # (!\inst4|clk_sortie\(5))) # (!\inst4|clk_sortie\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk_sortie\(4),
	datab => \inst4|clk_sortie\(6),
	datac => \inst4|clk_sortie\(3),
	datad => \inst4|clk_sortie\(5),
	combout => \inst4|LessThan0~0_combout\);

-- Location: LCCOMB_X114_Y43_N6
\inst4|LessThan0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|LessThan0~1_combout\ = (((\inst4|LessThan0~0_combout\) # (!\inst4|clk_sortie\(9))) # (!\inst4|clk_sortie\(8))) # (!\inst4|clk_sortie\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk_sortie\(7),
	datab => \inst4|clk_sortie\(8),
	datac => \inst4|clk_sortie\(9),
	datad => \inst4|LessThan0~0_combout\,
	combout => \inst4|LessThan0~1_combout\);

-- Location: LCCOMB_X114_Y42_N30
\inst4|LessThan0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|LessThan0~6_combout\ = (((!\inst4|LessThan0~1_combout\) # (!\inst4|LessThan0~5_combout\)) # (!\inst4|LessThan0~2_combout\)) # (!\inst4|LessThan0~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|LessThan0~3_combout\,
	datab => \inst4|LessThan0~2_combout\,
	datac => \inst4|LessThan0~5_combout\,
	datad => \inst4|LessThan0~1_combout\,
	combout => \inst4|LessThan0~6_combout\);

-- Location: FF_X114_Y42_N31
\inst4|clk6hz\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst4|LessThan0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|clk6hz~q\);

-- Location: CLKCTRL_G5
\inst4|clk6hz~clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst4|clk6hz~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst4|clk6hz~clkctrl_outclk\);

-- Location: IOIBUF_X115_Y40_N8
\ClockBascule~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_ClockBascule,
	o => \ClockBascule~input_o\);

-- Location: LCCOMB_X113_Y45_N24
\inst|Selector1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|Selector1~0_combout\ = (\inst4|clk6hz~q\ & (!\ClockBascule~input_o\ & ((\inst|Etat_present.E1~q\) # (!\inst|Etat_present.E0~q\)))) # (!\inst4|clk6hz~q\ & (((\inst|Etat_present.E1~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk6hz~q\,
	datab => \ClockBascule~input_o\,
	datac => \inst|Etat_present.E0~q\,
	datad => \inst|Etat_present.E1~q\,
	combout => \inst|Selector1~0_combout\);

-- Location: LCCOMB_X113_Y45_N28
\inst|Etat_present.E1~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|Etat_present.E1~feeder_combout\ = \inst|Selector1~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Selector1~0_combout\,
	combout => \inst|Etat_present.E1~feeder_combout\);

-- Location: FF_X113_Y45_N29
\inst|Etat_present.E1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst4|clk6hz~clkctrl_outclk\,
	d => \inst|Etat_present.E1~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|Etat_present.E1~q\);

-- Location: LCCOMB_X113_Y45_N14
\inst|Selector2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|Selector2~0_combout\ = (\inst4|clk6hz~q\ & (\ClockBascule~input_o\ & ((\inst|Etat_present.E2~q\) # (\inst|Etat_present.E1~q\)))) # (!\inst4|clk6hz~q\ & (((\inst|Etat_present.E2~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk6hz~q\,
	datab => \ClockBascule~input_o\,
	datac => \inst|Etat_present.E2~q\,
	datad => \inst|Etat_present.E1~q\,
	combout => \inst|Selector2~0_combout\);

-- Location: LCCOMB_X113_Y45_N30
\inst|Etat_present.E2~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|Etat_present.E2~feeder_combout\ = \inst|Selector2~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Selector2~0_combout\,
	combout => \inst|Etat_present.E2~feeder_combout\);

-- Location: FF_X113_Y45_N31
\inst|Etat_present.E2\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst4|clk6hz~clkctrl_outclk\,
	d => \inst|Etat_present.E2~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|Etat_present.E2~q\);

-- Location: LCCOMB_X113_Y45_N16
\inst|Selector3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|Selector3~0_combout\ = (\inst4|clk6hz~q\ & (!\ClockBascule~input_o\ & ((\inst|Etat_present.E2~q\) # (\inst|Etat_present.E3~q\)))) # (!\inst4|clk6hz~q\ & (((\inst|Etat_present.E3~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk6hz~q\,
	datab => \ClockBascule~input_o\,
	datac => \inst|Etat_present.E2~q\,
	datad => \inst|Etat_present.E3~q\,
	combout => \inst|Selector3~0_combout\);

-- Location: LCCOMB_X113_Y45_N12
\inst|Etat_present.E3~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|Etat_present.E3~feeder_combout\ = \inst|Selector3~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Selector3~0_combout\,
	combout => \inst|Etat_present.E3~feeder_combout\);

-- Location: FF_X113_Y45_N13
\inst|Etat_present.E3\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst4|clk6hz~clkctrl_outclk\,
	d => \inst|Etat_present.E3~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|Etat_present.E3~q\);

-- Location: LCCOMB_X113_Y45_N26
\inst|Selector0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|Selector0~0_combout\ = (\inst4|clk6hz~q\ & (((\inst|Etat_present.E0~q\ & !\inst|Etat_present.E3~q\)) # (!\ClockBascule~input_o\))) # (!\inst4|clk6hz~q\ & (((\inst|Etat_present.E0~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001011110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk6hz~q\,
	datab => \ClockBascule~input_o\,
	datac => \inst|Etat_present.E0~q\,
	datad => \inst|Etat_present.E3~q\,
	combout => \inst|Selector0~0_combout\);

-- Location: LCCOMB_X113_Y45_N22
\inst|Etat_present.E0~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|Etat_present.E0~feeder_combout\ = \inst|Selector0~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Selector0~0_combout\,
	combout => \inst|Etat_present.E0~feeder_combout\);

-- Location: FF_X113_Y45_N23
\inst|Etat_present.E0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst4|clk6hz~clkctrl_outclk\,
	d => \inst|Etat_present.E0~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|Etat_present.E0~q\);

-- Location: LCCOMB_X113_Y45_N20
\inst|updown~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst|updown~0_combout\ = (\inst|Etat_present.E3~q\) # (!\inst|Etat_present.E0~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|Etat_present.E0~q\,
	datad => \inst|Etat_present.E3~q\,
	combout => \inst|updown~0_combout\);

-- Location: LCCOMB_X114_Y45_N0
\inst3|LessThan0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst3|LessThan0~0_combout\ = ((!\inst3|s_compt_sortie\(0)) # (!\inst3|s_compt_sortie\(1))) # (!\inst3|s_compt_sortie\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst3|s_compt_sortie\(2),
	datac => \inst3|s_compt_sortie\(1),
	datad => \inst3|s_compt_sortie\(0),
	combout => \inst3|LessThan0~0_combout\);

-- Location: LCCOMB_X114_Y45_N24
\inst3|s_compt_sortie[2]~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst3|s_compt_sortie[2]~12_combout\ = (!\inst4|clk6hz~q\ & (\inst3|LessThan0~0_combout\ & (\inst3|LessThan1~0_combout\ & \inst|updown~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk6hz~q\,
	datab => \inst3|LessThan0~0_combout\,
	datac => \inst3|LessThan1~0_combout\,
	datad => \inst|updown~0_combout\,
	combout => \inst3|s_compt_sortie[2]~12_combout\);

-- Location: LCCOMB_X114_Y45_N14
\inst3|s_compt_sortie[0]~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst3|s_compt_sortie[0]~16_combout\ = (\inst|updown~0_combout\ & (((!\inst3|s_compt_sortie\(0))))) # (!\inst|updown~0_combout\ & (\inst3|LessThan0~0_combout\ & (\inst4|clk6hz~q\ $ (\inst3|s_compt_sortie\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001101100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk6hz~q\,
	datab => \inst3|s_compt_sortie\(0),
	datac => \inst3|LessThan0~0_combout\,
	datad => \inst|updown~0_combout\,
	combout => \inst3|s_compt_sortie[0]~16_combout\);

-- Location: LCCOMB_X114_Y45_N18
\inst1|equals~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst1|equals~0_combout\ = (\inst3|s_compt_sortie\(2) & (\inst3|s_compt_sortie\(1) & (\inst3|s_compt_sortie\(0) $ (!\inst|updown~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst3|s_compt_sortie\(2),
	datab => \inst3|s_compt_sortie\(0),
	datac => \inst3|s_compt_sortie\(1),
	datad => \inst|updown~0_combout\,
	combout => \inst1|equals~0_combout\);

-- Location: LCCOMB_X114_Y45_N28
\inst3|s_compt_sortie[0]~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst3|s_compt_sortie[0]~17_combout\ = (!\inst3|s_compt_sortie[2]~12_combout\ & ((\inst1|equals~0_combout\ & (\inst|updown~0_combout\)) # (!\inst1|equals~0_combout\ & ((\inst3|s_compt_sortie[0]~16_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst3|s_compt_sortie[2]~12_combout\,
	datab => \inst|updown~0_combout\,
	datac => \inst3|s_compt_sortie[0]~16_combout\,
	datad => \inst1|equals~0_combout\,
	combout => \inst3|s_compt_sortie[0]~17_combout\);

-- Location: LCCOMB_X114_Y45_N12
\inst3|s_compt_sortie[0]~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst3|s_compt_sortie[0]~18_combout\ = (\inst3|s_compt_sortie[0]~17_combout\) # ((\inst3|s_compt_sortie\(0) & \inst3|s_compt_sortie[2]~12_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst3|s_compt_sortie[0]~17_combout\,
	datac => \inst3|s_compt_sortie\(0),
	datad => \inst3|s_compt_sortie[2]~12_combout\,
	combout => \inst3|s_compt_sortie[0]~18_combout\);

-- Location: FF_X114_Y45_N13
\inst3|s_compt_sortie[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst3|s_compt_sortie[0]~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst3|s_compt_sortie\(0));

-- Location: LCCOMB_X114_Y45_N26
\inst3|LessThan1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst3|LessThan1~0_combout\ = (\inst3|s_compt_sortie\(1)) # ((\inst3|s_compt_sortie\(2)) # (\inst3|s_compt_sortie\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst3|s_compt_sortie\(1),
	datac => \inst3|s_compt_sortie\(2),
	datad => \inst3|s_compt_sortie\(0),
	combout => \inst3|LessThan1~0_combout\);

-- Location: LCCOMB_X114_Y45_N6
\inst3|s_compt_sortie~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst3|s_compt_sortie~13_combout\ = \inst3|s_compt_sortie\(1) $ (((\inst3|s_compt_sortie\(0) & ((\inst|updown~0_combout\) # (!\inst4|clk6hz~q\))) # (!\inst3|s_compt_sortie\(0) & ((!\inst|updown~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110010000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|clk6hz~q\,
	datab => \inst3|s_compt_sortie\(0),
	datac => \inst3|s_compt_sortie\(1),
	datad => \inst|updown~0_combout\,
	combout => \inst3|s_compt_sortie~13_combout\);

-- Location: LCCOMB_X114_Y45_N16
\inst3|s_compt_sortie[1]~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst3|s_compt_sortie[1]~14_combout\ = (!\inst3|s_compt_sortie~13_combout\ & ((\inst|updown~0_combout\ & (\inst3|LessThan1~0_combout\)) # (!\inst|updown~0_combout\ & ((\inst3|LessThan0~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst3|LessThan1~0_combout\,
	datab => \inst3|LessThan0~0_combout\,
	datac => \inst3|s_compt_sortie~13_combout\,
	datad => \inst|updown~0_combout\,
	combout => \inst3|s_compt_sortie[1]~14_combout\);

-- Location: LCCOMB_X114_Y45_N22
\inst3|s_compt_sortie[1]~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst3|s_compt_sortie[1]~15_combout\ = (\inst3|s_compt_sortie[2]~12_combout\ & (((\inst3|s_compt_sortie\(1))))) # (!\inst3|s_compt_sortie[2]~12_combout\ & (\inst3|s_compt_sortie[1]~14_combout\ & (!\inst1|equals~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst3|s_compt_sortie[1]~14_combout\,
	datab => \inst1|equals~0_combout\,
	datac => \inst3|s_compt_sortie\(1),
	datad => \inst3|s_compt_sortie[2]~12_combout\,
	combout => \inst3|s_compt_sortie[1]~15_combout\);

-- Location: FF_X114_Y45_N23
\inst3|s_compt_sortie[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst3|s_compt_sortie[1]~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst3|s_compt_sortie\(1));

-- Location: LCCOMB_X114_Y45_N30
\inst3|s_compt_sortie[2]~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst3|s_compt_sortie[2]~8_combout\ = (\inst3|s_compt_sortie\(2) & (\inst3|s_compt_sortie\(1) $ (((\inst|Etat_present.E0~q\ & !\inst|Etat_present.E3~q\))))) # (!\inst3|s_compt_sortie\(2) & (!\inst3|s_compt_sortie\(1) & ((\inst|Etat_present.E3~q\) # 
-- (!\inst|Etat_present.E0~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst3|s_compt_sortie\(2),
	datab => \inst|Etat_present.E0~q\,
	datac => \inst3|s_compt_sortie\(1),
	datad => \inst|Etat_present.E3~q\,
	combout => \inst3|s_compt_sortie[2]~8_combout\);

-- Location: LCCOMB_X114_Y45_N10
\inst3|s_compt_sortie[2]~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst3|s_compt_sortie[2]~19_combout\ = (\inst3|s_compt_sortie\(1) & (\inst3|s_compt_sortie\(2) & ((\inst|Etat_present.E3~q\) # (!\inst|Etat_present.E0~q\)))) # (!\inst3|s_compt_sortie\(1) & (((\inst3|s_compt_sortie\(2)) # (\inst|Etat_present.E3~q\)) # 
-- (!\inst|Etat_present.E0~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010101110001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst3|s_compt_sortie\(1),
	datab => \inst|Etat_present.E0~q\,
	datac => \inst3|s_compt_sortie\(2),
	datad => \inst|Etat_present.E3~q\,
	combout => \inst3|s_compt_sortie[2]~19_combout\);

-- Location: LCCOMB_X114_Y45_N8
\inst3|s_compt_sortie[2]~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst3|s_compt_sortie[2]~20_combout\ = (\inst4|clk6hz~q\ & (((\inst3|s_compt_sortie\(0))))) # (!\inst4|clk6hz~q\ & (\inst3|s_compt_sortie[2]~19_combout\ & ((\inst3|s_compt_sortie\(2)) # (!\inst3|s_compt_sortie\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst3|s_compt_sortie\(2),
	datab => \inst3|s_compt_sortie\(0),
	datac => \inst4|clk6hz~q\,
	datad => \inst3|s_compt_sortie[2]~19_combout\,
	combout => \inst3|s_compt_sortie[2]~20_combout\);

-- Location: LCCOMB_X114_Y45_N20
\inst3|s_compt_sortie[2]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst3|s_compt_sortie[2]~9_combout\ = \inst3|s_compt_sortie\(2) $ (((\inst|Etat_present.E0~q\ & (\inst3|s_compt_sortie\(1) & !\inst|Etat_present.E3~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101001101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst3|s_compt_sortie\(2),
	datab => \inst|Etat_present.E0~q\,
	datac => \inst3|s_compt_sortie\(1),
	datad => \inst|Etat_present.E3~q\,
	combout => \inst3|s_compt_sortie[2]~9_combout\);

-- Location: LCCOMB_X114_Y45_N4
\inst3|s_compt_sortie[2]~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst3|s_compt_sortie[2]~11_combout\ = (\inst4|clk6hz~q\ & ((\inst3|s_compt_sortie[2]~20_combout\ & ((\inst3|s_compt_sortie[2]~9_combout\))) # (!\inst3|s_compt_sortie[2]~20_combout\ & (\inst3|s_compt_sortie[2]~8_combout\)))) # (!\inst4|clk6hz~q\ & 
-- (((\inst3|s_compt_sortie[2]~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst3|s_compt_sortie[2]~8_combout\,
	datab => \inst4|clk6hz~q\,
	datac => \inst3|s_compt_sortie[2]~20_combout\,
	datad => \inst3|s_compt_sortie[2]~9_combout\,
	combout => \inst3|s_compt_sortie[2]~11_combout\);

-- Location: FF_X114_Y45_N5
\inst3|s_compt_sortie[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \Clock50MHz~inputclkctrl_outclk\,
	d => \inst3|s_compt_sortie[2]~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst3|s_compt_sortie\(2));

-- Location: LCCOMB_X114_Y50_N28
\inst2|output[5]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|output[5]~0_combout\ = (\inst3|s_compt_sortie\(2) & ((!\inst3|s_compt_sortie\(1)))) # (!\inst3|s_compt_sortie\(2) & ((\inst3|s_compt_sortie\(0)) # (\inst3|s_compt_sortie\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst3|s_compt_sortie\(0),
	datac => \inst3|s_compt_sortie\(2),
	datad => \inst3|s_compt_sortie\(1),
	combout => \inst2|output[5]~0_combout\);

-- Location: LCCOMB_X114_Y49_N12
\inst2|output[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|output\(5) = (\inst3|LessThan1~0_combout\ & ((\inst2|output[5]~0_combout\) # (\inst2|output\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst2|output[5]~0_combout\,
	datac => \inst3|LessThan1~0_combout\,
	datad => \inst2|output\(5),
	combout => \inst2|output\(5));

-- Location: LCCOMB_X114_Y50_N12
\inst2|output[5]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|output[5]~1_combout\ = (!\inst3|s_compt_sortie\(1)) # (!\inst3|s_compt_sortie\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst3|s_compt_sortie\(2),
	datad => \inst3|s_compt_sortie\(1),
	combout => \inst2|output[5]~1_combout\);

-- Location: LCCOMB_X114_Y50_N14
\inst2|comb~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|comb~0_combout\ = ((\inst3|s_compt_sortie\(2)) # (\inst3|s_compt_sortie\(1))) # (!\inst3|s_compt_sortie\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst3|s_compt_sortie\(0),
	datac => \inst3|s_compt_sortie\(2),
	datad => \inst3|s_compt_sortie\(1),
	combout => \inst2|comb~0_combout\);

-- Location: LCCOMB_X114_Y50_N6
\inst2|output[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|output\(4) = (\inst2|comb~0_combout\ & ((\inst2|output[5]~1_combout\) # (\inst2|output\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|output[5]~1_combout\,
	datac => \inst2|comb~0_combout\,
	datad => \inst2|output\(4),
	combout => \inst2|output\(4));

-- Location: LCCOMB_X114_Y50_N8
\inst2|comb~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|comb~2_combout\ = ((\inst3|s_compt_sortie\(0) & !\inst3|s_compt_sortie\(2))) # (!\inst3|s_compt_sortie\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst3|s_compt_sortie\(0),
	datac => \inst3|s_compt_sortie\(2),
	datad => \inst3|s_compt_sortie\(1),
	combout => \inst2|comb~2_combout\);

-- Location: LCCOMB_X114_Y50_N30
\inst2|comb~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|comb~1_combout\ = (!\inst3|s_compt_sortie\(0) & (!\inst3|s_compt_sortie\(2) & \inst3|s_compt_sortie\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst3|s_compt_sortie\(0),
	datac => \inst3|s_compt_sortie\(2),
	datad => \inst3|s_compt_sortie\(1),
	combout => \inst2|comb~1_combout\);

-- Location: LCCOMB_X114_Y50_N20
\inst2|output[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|output\(3) = (!\inst2|comb~1_combout\ & ((\inst2|comb~2_combout\) # (\inst2|output\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst2|comb~2_combout\,
	datac => \inst2|comb~1_combout\,
	datad => \inst2|output\(3),
	combout => \inst2|output\(3));

-- Location: LCCOMB_X114_Y50_N0
\inst2|comb~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|comb~4_combout\ = ((!\inst3|s_compt_sortie\(0) & !\inst3|s_compt_sortie\(2))) # (!\inst3|s_compt_sortie\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst3|s_compt_sortie\(0),
	datac => \inst3|s_compt_sortie\(2),
	datad => \inst3|s_compt_sortie\(1),
	combout => \inst2|comb~4_combout\);

-- Location: LCCOMB_X114_Y50_N22
\inst2|comb~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|comb~3_combout\ = (\inst3|s_compt_sortie\(0) & (!\inst3|s_compt_sortie\(2) & \inst3|s_compt_sortie\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst3|s_compt_sortie\(0),
	datac => \inst3|s_compt_sortie\(2),
	datad => \inst3|s_compt_sortie\(1),
	combout => \inst2|comb~3_combout\);

-- Location: LCCOMB_X114_Y50_N2
\inst2|output[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|output\(2) = (!\inst2|comb~3_combout\ & ((\inst2|comb~4_combout\) # (\inst2|output\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst2|comb~4_combout\,
	datac => \inst2|comb~3_combout\,
	datad => \inst2|output\(2),
	combout => \inst2|output\(2));

-- Location: LCCOMB_X114_Y50_N18
\inst2|comb~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|comb~5_combout\ = (!\inst3|s_compt_sortie\(0) & (\inst3|s_compt_sortie\(2) & !\inst3|s_compt_sortie\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst3|s_compt_sortie\(0),
	datac => \inst3|s_compt_sortie\(2),
	datad => \inst3|s_compt_sortie\(1),
	combout => \inst2|comb~5_combout\);

-- Location: LCCOMB_X114_Y50_N4
\inst2|comb~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|comb~6_combout\ = ((\inst3|s_compt_sortie\(0) & !\inst3|s_compt_sortie\(1))) # (!\inst3|s_compt_sortie\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst3|s_compt_sortie\(0),
	datac => \inst3|s_compt_sortie\(2),
	datad => \inst3|s_compt_sortie\(1),
	combout => \inst2|comb~6_combout\);

-- Location: LCCOMB_X114_Y50_N16
\inst2|output[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|output\(1) = (!\inst2|comb~5_combout\ & ((\inst2|comb~6_combout\) # (\inst2|output\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst2|comb~5_combout\,
	datac => \inst2|comb~6_combout\,
	datad => \inst2|output\(1),
	combout => \inst2|output\(1));

-- Location: LCCOMB_X114_Y50_N24
\inst2|comb~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|comb~8_combout\ = ((!\inst3|s_compt_sortie\(0) & !\inst3|s_compt_sortie\(1))) # (!\inst3|s_compt_sortie\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst3|s_compt_sortie\(0),
	datac => \inst3|s_compt_sortie\(2),
	datad => \inst3|s_compt_sortie\(1),
	combout => \inst2|comb~8_combout\);

-- Location: LCCOMB_X114_Y50_N26
\inst2|comb~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|comb~7_combout\ = (\inst3|s_compt_sortie\(0) & (\inst3|s_compt_sortie\(2) & !\inst3|s_compt_sortie\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst3|s_compt_sortie\(0),
	datac => \inst3|s_compt_sortie\(2),
	datad => \inst3|s_compt_sortie\(1),
	combout => \inst2|comb~7_combout\);

-- Location: LCCOMB_X114_Y50_N10
\inst2|output[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst2|output\(0) = (!\inst2|comb~7_combout\ & ((\inst2|comb~8_combout\) # (\inst2|output\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst2|comb~8_combout\,
	datac => \inst2|comb~7_combout\,
	datad => \inst2|output\(0),
	combout => \inst2|output\(0));

ww_BasculeOut <= \BasculeOut~output_o\;

ww_OutCompteur(2) <= \OutCompteur[2]~output_o\;

ww_OutCompteur(1) <= \OutCompteur[1]~output_o\;

ww_OutCompteur(0) <= \OutCompteur[0]~output_o\;

ww_sortie(5) <= \sortie[5]~output_o\;

ww_sortie(4) <= \sortie[4]~output_o\;

ww_sortie(3) <= \sortie[3]~output_o\;

ww_sortie(2) <= \sortie[2]~output_o\;

ww_sortie(1) <= \sortie[1]~output_o\;

ww_sortie(0) <= \sortie[0]~output_o\;
END structure;


