LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;


ENTITY TP4_DiviseurFrequence IS
 PORT (
       clk50Mhz : IN STD_LOGIC;
       clk_sortie : BUFFER STD_LOGIC_VECTOR(23 DOWNTO 0);
		 clk6hz : OUT STD_LOGIC
		);
END TP4_DiviseurFrequence;

ARCHITECTURE rtl OF TP4_DiviseurFrequence IS 
	-- SIGNAL count_6hz : STD_LOGIC_VECTOR(23 DOWNTO 0);
	begin
	process
	begin
	
	WAIT UNTIL clk50Mhz'EVENT and clk50Mhz = '1';
	
	-- if clk_sortie < "011111110010100000010101" then 
	if clk_sortie < "000000000000001111101000" then
	clk_sortie <= clk_sortie + 1;
	clk6hz <= '0' ;  
	else
	clk_sortie <= "000000000000000000000000"; 
	clk6hz <= '1';
	
	end if;
 END process;
 end;