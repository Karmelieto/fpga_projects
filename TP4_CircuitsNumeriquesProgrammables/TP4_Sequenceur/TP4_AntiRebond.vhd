LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;


ENTITY TP4_AntiRebond IS
 PORT (
		clk: IN STD_LOGIC;
   	entree_bouton, clk_enable : IN STD_LOGIC;
   	updown : OUT STD_LOGIC
   	 );
END TP4_AntiRebond;

ARCHITECTURE rtl OF TP4_AntiRebond IS

TYPE Etat is (E0, E1, E2, E3);
Signal Etat_present : Etat; 
Signal Etat_futur : Etat;

BEGIN
Sequentiel_maj_etat : process (clk)
begin
	if clk'event and clk = '1' then
		Etat_present <= Etat_futur;
		end if;
end process Sequentiel_maj_etat;


Combinatoire_etats : process (entree_bouton, clk_enable, Etat_present)
begin
case Etat_present is
	when E0 => if entree_bouton = '0' AND clk_enable='1' then
						Etat_futur <= E1;
					else
		Etat_futur <= E0;
					end if;		
	when E1 => if entree_bouton = '1' AND clk_enable='1' then
						Etat_futur <= E2;
					else
		Etat_futur <= E1;
					end if;	
	when E2 => if entree_bouton = '0' AND clk_enable='1' then
						Etat_futur <= E3;
					else
		Etat_futur <= E2;
					end if;	
	when E3 => if entree_bouton = '1' AND clk_enable='1' then
						Etat_futur <= E0;
					else
		Etat_futur <= E3;
					end if;	
end case;

end process Combinatoire_etats;

Combinatoire_sorties : process (Etat_present)
begin
	case Etat_present is
		when E0 => updown <= '0';
		when E1 => updown <= '1';
		when E2 => updown <= '1';
		when E3 => updown <= '0';
end case;
end process Combinatoire_sorties;
end ;
