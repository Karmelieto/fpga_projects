library IEEE;
use IEEE.STD_LOGIC_1164.all;

ENTITY TP4_Decodeur IS
port(
 input : in STD_LOGIC_VECTOR(2 downto 0);
 output : out STD_LOGIC_VECTOR(5 downto 0)
);
END TP4_Decodeur;


ARCHITECTURE behaviour OF TP4_Decodeur IS
BEGIN PROCESS(input)
BEGIN
if(input="000") then
 output <= NOT "100000";
elsif(input="001") then
 output <= NOT "010000";
elsif(input="010") then
 output <= NOT "001000";
elsif(input="011") then
 output <= NOT "000100";
elsif(input="100") then
 output <= NOT "000010";
elsif(input="101") then
 output <= NOT "000001";
end if;
end PROCESS;
END behaviour;
