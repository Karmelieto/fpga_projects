LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;


ENTITY TP4_CompteurSimple IS
 PORT (
       clk, clk_enable, reset : IN STD_LOGIC;
       compt_sortie : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
		);
END TP4_CompteurSimple;

ARCHITECTURE rtl OF TP4_CompteurSimple IS 
	SIGNAL s_compt_sortie : STD_LOGIC_VECTOR(2 DOWNTO 0);
BEGIN
PROCESS(clk)
BEGIN
 IF clk'EVENT AND clk = '1' THEN
	IF reset = '1' THEN 
		s_compt_sortie <= "000";
	ELSE 	
		IF s_compt_sortie >= 7 THEN
				s_compt_sortie <= "000";
		ELSE
			IF clk_enable = '1' then
				s_compt_sortie <= s_compt_sortie + 1;
				END IF;
		END IF;
		END IF;
 ELSE s_compt_sortie <= s_compt_sortie;
 END IF;
 END PROCESS;
	compt_sortie <= s_compt_sortie;
END;
