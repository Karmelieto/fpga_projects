LIBRARY ieee;    
USE ieee.std_logic_1164.all;    
 
ENTITY TP4_Cascade_CPT_DECO IS PORT  
(clk_C: IN STD_LOGIC; 
test : out STD_LOGIC_VECTOR(2 downto 0);
output : out STD_LOGIC_VECTOR(5 downto 0));   
END TP4_Cascade_CPT_DECO;    
 
ARCHITECTURE pure_logic OF TP4_Cascade_CPT_DECO IS

component TP4_CompteurSimple
	port (clk, reset : IN STD_LOGIC;
	compt_sortie : out STD_LOGIC_VECTOR(2 downto 0)
	);
end component;
 
component TP4_Decodeur
 PORT (
      input : in STD_LOGIC_VECTOR(2 downto 0);
		output : out STD_LOGIC_VECTOR(5 downto 0)
);
END component;
 

signal output_compteur :  STD_LOGIC_VECTOR(2 downto 0);

BEGIN

c1 : TP4_CompteurSimple port map(clk_C,'0',output_compteur); 
 
c2 : TP4_Decodeur port map(output_compteur, output);

test <= output_compteur;
 
END pure_logic;
