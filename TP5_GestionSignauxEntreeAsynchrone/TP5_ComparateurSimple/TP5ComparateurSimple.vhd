library IEEE;
use IEEE.std_logic_1164.all;

entity TP5ComparateurSimple is
	port (a, b: in std_logic_vector(4 downto 0);
	equals : out std_logic);
end TP5ComparateurSimple;

architecture dataflow of TP5ComparateurSimple is
begin
	equals <= '1' when (a = b) else '0';
end dataflow;