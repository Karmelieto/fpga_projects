-- Copyright (C) 2019  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 19.1.0 Build 670 09/22/2019 SJ Standard Edition"

-- DATE "05/06/2020 08:46:16"

-- 
-- Device: Altera EP4CE115F29I8L Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCEO~	=>  Location: PIN_P28,	 I/O Standard: 2.5 V,	 Current Strength: 8mA


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	TP5_enable IS
    PORT (
	ck : IN std_logic;
	s_out : OUT std_logic_vector(4 DOWNTO 0)
	);
END TP5_enable;

-- Design Ports Information
-- s_out[0]	=>  Location: PIN_T3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- s_out[1]	=>  Location: PIN_T7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- s_out[2]	=>  Location: PIN_R4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- s_out[3]	=>  Location: PIN_R5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- s_out[4]	=>  Location: PIN_T4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ck	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF TP5_enable IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_ck : std_logic;
SIGNAL ww_s_out : std_logic_vector(4 DOWNTO 0);
SIGNAL \ck~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \s_out[0]~output_o\ : std_logic;
SIGNAL \s_out[1]~output_o\ : std_logic;
SIGNAL \s_out[2]~output_o\ : std_logic;
SIGNAL \s_out[3]~output_o\ : std_logic;
SIGNAL \s_out[4]~output_o\ : std_logic;
SIGNAL \ck~input_o\ : std_logic;
SIGNAL \ck~inputclkctrl_outclk\ : std_logic;
SIGNAL \c3|s_compt_sortie[0]~5_combout\ : std_logic;
SIGNAL \c1|s_compt_sortie[0]~5_combout\ : std_logic;
SIGNAL \c1|s_compt_sortie[0]~6\ : std_logic;
SIGNAL \c1|s_compt_sortie[1]~7_combout\ : std_logic;
SIGNAL \c1|s_compt_sortie[1]~8\ : std_logic;
SIGNAL \c1|s_compt_sortie[2]~9_combout\ : std_logic;
SIGNAL \c1|s_compt_sortie[2]~10\ : std_logic;
SIGNAL \c1|s_compt_sortie[3]~11_combout\ : std_logic;
SIGNAL \c1|s_compt_sortie[3]~12\ : std_logic;
SIGNAL \c1|s_compt_sortie[4]~13_combout\ : std_logic;
SIGNAL \c2|Equal0~0_combout\ : std_logic;
SIGNAL \c2|Equal0~1_combout\ : std_logic;
SIGNAL \c3|s_compt_sortie[0]~6\ : std_logic;
SIGNAL \c3|s_compt_sortie[1]~7_combout\ : std_logic;
SIGNAL \c3|s_compt_sortie[1]~8\ : std_logic;
SIGNAL \c3|s_compt_sortie[2]~9_combout\ : std_logic;
SIGNAL \c3|s_compt_sortie[2]~10\ : std_logic;
SIGNAL \c3|s_compt_sortie[3]~11_combout\ : std_logic;
SIGNAL \c3|s_compt_sortie[3]~12\ : std_logic;
SIGNAL \c3|s_compt_sortie[4]~13_combout\ : std_logic;
SIGNAL \c3|s_compt_sortie\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \c1|s_compt_sortie\ : std_logic_vector(4 DOWNTO 0);

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_ck <= ck;
s_out <= ww_s_out;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\ck~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \ck~input_o\);
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X0_Y32_N16
\s_out[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \c3|s_compt_sortie\(0),
	devoe => ww_devoe,
	o => \s_out[0]~output_o\);

-- Location: IOOBUF_X0_Y31_N16
\s_out[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \c3|s_compt_sortie\(1),
	devoe => ww_devoe,
	o => \s_out[1]~output_o\);

-- Location: IOOBUF_X0_Y33_N16
\s_out[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \c3|s_compt_sortie\(2),
	devoe => ww_devoe,
	o => \s_out[2]~output_o\);

-- Location: IOOBUF_X0_Y32_N23
\s_out[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \c3|s_compt_sortie\(3),
	devoe => ww_devoe,
	o => \s_out[3]~output_o\);

-- Location: IOOBUF_X0_Y33_N23
\s_out[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \c3|s_compt_sortie\(4),
	devoe => ww_devoe,
	o => \s_out[4]~output_o\);

-- Location: IOIBUF_X0_Y36_N8
\ck~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_ck,
	o => \ck~input_o\);

-- Location: CLKCTRL_G2
\ck~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \ck~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \ck~inputclkctrl_outclk\);

-- Location: LCCOMB_X1_Y32_N4
\c3|s_compt_sortie[0]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \c3|s_compt_sortie[0]~5_combout\ = \c3|s_compt_sortie\(0) $ (VCC)
-- \c3|s_compt_sortie[0]~6\ = CARRY(\c3|s_compt_sortie\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \c3|s_compt_sortie\(0),
	datad => VCC,
	combout => \c3|s_compt_sortie[0]~5_combout\,
	cout => \c3|s_compt_sortie[0]~6\);

-- Location: LCCOMB_X1_Y32_N18
\c1|s_compt_sortie[0]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \c1|s_compt_sortie[0]~5_combout\ = \c1|s_compt_sortie\(0) $ (VCC)
-- \c1|s_compt_sortie[0]~6\ = CARRY(\c1|s_compt_sortie\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \c1|s_compt_sortie\(0),
	datad => VCC,
	combout => \c1|s_compt_sortie[0]~5_combout\,
	cout => \c1|s_compt_sortie[0]~6\);

-- Location: FF_X1_Y32_N19
\c1|s_compt_sortie[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ck~inputclkctrl_outclk\,
	d => \c1|s_compt_sortie[0]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \c1|s_compt_sortie\(0));

-- Location: LCCOMB_X1_Y32_N20
\c1|s_compt_sortie[1]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \c1|s_compt_sortie[1]~7_combout\ = (\c1|s_compt_sortie\(1) & (!\c1|s_compt_sortie[0]~6\)) # (!\c1|s_compt_sortie\(1) & ((\c1|s_compt_sortie[0]~6\) # (GND)))
-- \c1|s_compt_sortie[1]~8\ = CARRY((!\c1|s_compt_sortie[0]~6\) # (!\c1|s_compt_sortie\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \c1|s_compt_sortie\(1),
	datad => VCC,
	cin => \c1|s_compt_sortie[0]~6\,
	combout => \c1|s_compt_sortie[1]~7_combout\,
	cout => \c1|s_compt_sortie[1]~8\);

-- Location: FF_X1_Y32_N21
\c1|s_compt_sortie[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ck~inputclkctrl_outclk\,
	d => \c1|s_compt_sortie[1]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \c1|s_compt_sortie\(1));

-- Location: LCCOMB_X1_Y32_N22
\c1|s_compt_sortie[2]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \c1|s_compt_sortie[2]~9_combout\ = (\c1|s_compt_sortie\(2) & (\c1|s_compt_sortie[1]~8\ $ (GND))) # (!\c1|s_compt_sortie\(2) & (!\c1|s_compt_sortie[1]~8\ & VCC))
-- \c1|s_compt_sortie[2]~10\ = CARRY((\c1|s_compt_sortie\(2) & !\c1|s_compt_sortie[1]~8\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \c1|s_compt_sortie\(2),
	datad => VCC,
	cin => \c1|s_compt_sortie[1]~8\,
	combout => \c1|s_compt_sortie[2]~9_combout\,
	cout => \c1|s_compt_sortie[2]~10\);

-- Location: FF_X1_Y32_N23
\c1|s_compt_sortie[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ck~inputclkctrl_outclk\,
	d => \c1|s_compt_sortie[2]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \c1|s_compt_sortie\(2));

-- Location: LCCOMB_X1_Y32_N24
\c1|s_compt_sortie[3]~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \c1|s_compt_sortie[3]~11_combout\ = (\c1|s_compt_sortie\(3) & (!\c1|s_compt_sortie[2]~10\)) # (!\c1|s_compt_sortie\(3) & ((\c1|s_compt_sortie[2]~10\) # (GND)))
-- \c1|s_compt_sortie[3]~12\ = CARRY((!\c1|s_compt_sortie[2]~10\) # (!\c1|s_compt_sortie\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \c1|s_compt_sortie\(3),
	datad => VCC,
	cin => \c1|s_compt_sortie[2]~10\,
	combout => \c1|s_compt_sortie[3]~11_combout\,
	cout => \c1|s_compt_sortie[3]~12\);

-- Location: FF_X1_Y32_N25
\c1|s_compt_sortie[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ck~inputclkctrl_outclk\,
	d => \c1|s_compt_sortie[3]~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \c1|s_compt_sortie\(3));

-- Location: LCCOMB_X1_Y32_N26
\c1|s_compt_sortie[4]~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \c1|s_compt_sortie[4]~13_combout\ = \c1|s_compt_sortie\(4) $ (!\c1|s_compt_sortie[3]~12\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \c1|s_compt_sortie\(4),
	cin => \c1|s_compt_sortie[3]~12\,
	combout => \c1|s_compt_sortie[4]~13_combout\);

-- Location: FF_X1_Y32_N27
\c1|s_compt_sortie[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ck~inputclkctrl_outclk\,
	d => \c1|s_compt_sortie[4]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \c1|s_compt_sortie\(4));

-- Location: LCCOMB_X1_Y32_N16
\c2|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \c2|Equal0~0_combout\ = (\c1|s_compt_sortie\(2) & (\c1|s_compt_sortie\(3) & (\c1|s_compt_sortie\(1) & \c1|s_compt_sortie\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \c1|s_compt_sortie\(2),
	datab => \c1|s_compt_sortie\(3),
	datac => \c1|s_compt_sortie\(1),
	datad => \c1|s_compt_sortie\(0),
	combout => \c2|Equal0~0_combout\);

-- Location: LCCOMB_X1_Y32_N2
\c2|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \c2|Equal0~1_combout\ = (\c1|s_compt_sortie\(4) & \c2|Equal0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \c1|s_compt_sortie\(4),
	datad => \c2|Equal0~0_combout\,
	combout => \c2|Equal0~1_combout\);

-- Location: FF_X1_Y32_N5
\c3|s_compt_sortie[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ck~inputclkctrl_outclk\,
	d => \c3|s_compt_sortie[0]~5_combout\,
	ena => \c2|Equal0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \c3|s_compt_sortie\(0));

-- Location: LCCOMB_X1_Y32_N6
\c3|s_compt_sortie[1]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \c3|s_compt_sortie[1]~7_combout\ = (\c3|s_compt_sortie\(1) & (!\c3|s_compt_sortie[0]~6\)) # (!\c3|s_compt_sortie\(1) & ((\c3|s_compt_sortie[0]~6\) # (GND)))
-- \c3|s_compt_sortie[1]~8\ = CARRY((!\c3|s_compt_sortie[0]~6\) # (!\c3|s_compt_sortie\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \c3|s_compt_sortie\(1),
	datad => VCC,
	cin => \c3|s_compt_sortie[0]~6\,
	combout => \c3|s_compt_sortie[1]~7_combout\,
	cout => \c3|s_compt_sortie[1]~8\);

-- Location: FF_X1_Y32_N7
\c3|s_compt_sortie[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ck~inputclkctrl_outclk\,
	d => \c3|s_compt_sortie[1]~7_combout\,
	ena => \c2|Equal0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \c3|s_compt_sortie\(1));

-- Location: LCCOMB_X1_Y32_N8
\c3|s_compt_sortie[2]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \c3|s_compt_sortie[2]~9_combout\ = (\c3|s_compt_sortie\(2) & (\c3|s_compt_sortie[1]~8\ $ (GND))) # (!\c3|s_compt_sortie\(2) & (!\c3|s_compt_sortie[1]~8\ & VCC))
-- \c3|s_compt_sortie[2]~10\ = CARRY((\c3|s_compt_sortie\(2) & !\c3|s_compt_sortie[1]~8\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \c3|s_compt_sortie\(2),
	datad => VCC,
	cin => \c3|s_compt_sortie[1]~8\,
	combout => \c3|s_compt_sortie[2]~9_combout\,
	cout => \c3|s_compt_sortie[2]~10\);

-- Location: FF_X1_Y32_N9
\c3|s_compt_sortie[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ck~inputclkctrl_outclk\,
	d => \c3|s_compt_sortie[2]~9_combout\,
	ena => \c2|Equal0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \c3|s_compt_sortie\(2));

-- Location: LCCOMB_X1_Y32_N10
\c3|s_compt_sortie[3]~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \c3|s_compt_sortie[3]~11_combout\ = (\c3|s_compt_sortie\(3) & (!\c3|s_compt_sortie[2]~10\)) # (!\c3|s_compt_sortie\(3) & ((\c3|s_compt_sortie[2]~10\) # (GND)))
-- \c3|s_compt_sortie[3]~12\ = CARRY((!\c3|s_compt_sortie[2]~10\) # (!\c3|s_compt_sortie\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \c3|s_compt_sortie\(3),
	datad => VCC,
	cin => \c3|s_compt_sortie[2]~10\,
	combout => \c3|s_compt_sortie[3]~11_combout\,
	cout => \c3|s_compt_sortie[3]~12\);

-- Location: FF_X1_Y32_N11
\c3|s_compt_sortie[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ck~inputclkctrl_outclk\,
	d => \c3|s_compt_sortie[3]~11_combout\,
	ena => \c2|Equal0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \c3|s_compt_sortie\(3));

-- Location: LCCOMB_X1_Y32_N12
\c3|s_compt_sortie[4]~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \c3|s_compt_sortie[4]~13_combout\ = \c3|s_compt_sortie[3]~12\ $ (!\c3|s_compt_sortie\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \c3|s_compt_sortie\(4),
	cin => \c3|s_compt_sortie[3]~12\,
	combout => \c3|s_compt_sortie[4]~13_combout\);

-- Location: FF_X1_Y32_N13
\c3|s_compt_sortie[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ck~inputclkctrl_outclk\,
	d => \c3|s_compt_sortie[4]~13_combout\,
	ena => \c2|Equal0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \c3|s_compt_sortie\(4));

ww_s_out(0) <= \s_out[0]~output_o\;

ww_s_out(1) <= \s_out[1]~output_o\;

ww_s_out(2) <= \s_out[2]~output_o\;

ww_s_out(3) <= \s_out[3]~output_o\;

ww_s_out(4) <= \s_out[4]~output_o\;
END structure;


