LIBRARY ieee;    
USE ieee.std_logic_1164.all;    
 
ENTITY TP5_enable IS PORT  
(ck : in std_logic;  
SIGNAL s_out : out STD_LOGIC_VECTOR(4 DOWNTO 0));   
END TP5_enable;    
 
ARCHITECTURE pure_logic OF TP5_enable IS
 
component CompteurSimple
 PORT (
       clk : IN STD_LOGIC;
       compt_sortie : out STD_LOGIC_VECTOR(4 DOWNTO 0)
);
END component;
 
component ComparateurSimple
	port (a : in std_logic_vector(4 downto 0);
	equals : out std_logic);
end component;

component Compteur_enable
	port (clk, enable : IN STD_LOGIC;
			compteur_sortie : out std_logic_vector(4 downto 0)
			);
			end component;
 
signal m1 : std_logic; 
 
signal out_compteur : STD_LOGIC_VECTOR(4 DOWNTO 0);
 
BEGIN
 
c1 : CompteurSimple port map(ck, out_compteur);
 
c2 : ComparateurSimple port map(out_compteur, m1);
 
c3 : Compteur_enable port map(ck,m1, s_out);
 
 
END pure_logic;
