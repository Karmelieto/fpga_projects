LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;


ENTITY Compteur_enable IS
 PORT (
       clk, enable : IN STD_LOGIC;
       compteur_sortie : OUT STD_LOGIC_VECTOR(4 DOWNTO 0)
		);
END Compteur_enable;

ARCHITECTURE rtl OF Compteur_enable IS 
	SIGNAL s_compt_sortie : STD_LOGIC_VECTOR(4 DOWNTO 0);
BEGIN
PROCESS(clk)
BEGIN
IF enable = '1' THEN 
 IF clk'EVENT AND clk = '1' THEN
   IF s_compt_sortie >= 31 THEN
         s_compt_sortie <= "00000";
   ELSE
         s_compt_sortie <= s_compt_sortie + 1;
   END IF;
 END IF;
 ELSE s_compt_sortie <= s_compt_sortie;
 END IF;
END PROCESS;
	compteur_sortie <= s_compt_sortie;
END;
