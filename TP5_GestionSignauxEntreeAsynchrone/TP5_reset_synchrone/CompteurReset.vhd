LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;


ENTITY CompteurReset IS
 PORT (
       clk, reset : IN STD_LOGIC;
       compteur_sortie : OUT STD_LOGIC_VECTOR(4 DOWNTO 0)
		);
END CompteurReset;

ARCHITECTURE rtl OF CompteurReset IS 
	SIGNAL s_compt_sortie : STD_LOGIC_VECTOR(4 DOWNTO 0);
BEGIN
PROCESS(clk)
BEGIN
 IF clk'EVENT AND clk = '1' THEN
	IF reset = '1' THEN 
		s_compt_sortie <= "00000";
	ELSE 	
		IF s_compt_sortie >= 31 THEN
				s_compt_sortie <= "00000";
		ELSE
				s_compt_sortie <= s_compt_sortie + 1;
		END IF;
		END IF;
 ELSE s_compt_sortie <= s_compt_sortie;
 END IF;
 END PROCESS;
	compteur_sortie <= s_compt_sortie;
END;
