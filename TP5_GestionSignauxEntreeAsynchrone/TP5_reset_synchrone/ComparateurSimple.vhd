library IEEE;
use IEEE.std_logic_1164.all;

entity ComparateurSimple is
	port (a: in std_logic_vector(4 downto 0);
	equals : out std_logic);
end ComparateurSimple;

architecture dataflow of ComparateurSimple is
begin
	equals <= '1' when (a =  "11111") else '0';
end dataflow;
