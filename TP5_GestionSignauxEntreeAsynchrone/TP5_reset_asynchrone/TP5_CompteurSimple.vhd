LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;


ENTITY TP5_CompteurSimple IS
 PORT (
       clk, rst : IN STD_LOGIC;
       compt_sortie : OUT STD_LOGIC_VECTOR(4 DOWNTO 0)
		);
END TP5_CompteurSimple;

ARCHITECTURE rtl OF TP5_CompteurSimple IS 
	SIGNAL s_compt_sortie : STD_LOGIC_VECTOR(4 DOWNTO 0);
BEGIN
PROCESS(clk)
BEGIN
IF rst = '0' THEN
 IF clk'EVENT AND clk = '1' THEN
   IF s_compt_sortie >= 31 THEN
         s_compt_sortie <= "00000";
   ELSE
         s_compt_sortie <= s_compt_sortie + 1;
   END IF;
 END IF;
 ELSE s_compt_sortie <= "00000"; 
END IF;
END PROCESS;
	compt_sortie <= s_compt_sortie;
END;
