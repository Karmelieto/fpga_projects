-- Copyright (C) 2019  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 19.1.0 Build 670 09/22/2019 SJ Lite Edition"

-- DATE "05/07/2020 16:42:01"

-- 
-- Device: Altera EP4CE115F29C7 Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCEO~	=>  Location: PIN_P28,	 I/O Standard: 2.5 V,	 Current Strength: 8mA


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	TP5_CompteurSimple IS
    PORT (
	clk : IN std_logic;
	rst : IN std_logic;
	compt_sortie : BUFFER std_logic_vector(4 DOWNTO 0)
	);
END TP5_CompteurSimple;

-- Design Ports Information
-- compt_sortie[0]	=>  Location: PIN_G2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- compt_sortie[1]	=>  Location: PIN_K2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- compt_sortie[2]	=>  Location: PIN_K1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- compt_sortie[3]	=>  Location: PIN_K3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- compt_sortie[4]	=>  Location: PIN_G1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rst	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF TP5_CompteurSimple IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_rst : std_logic;
SIGNAL ww_compt_sortie : std_logic_vector(4 DOWNTO 0);
SIGNAL \rst~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \compt_sortie[0]~output_o\ : std_logic;
SIGNAL \compt_sortie[1]~output_o\ : std_logic;
SIGNAL \compt_sortie[2]~output_o\ : std_logic;
SIGNAL \compt_sortie[3]~output_o\ : std_logic;
SIGNAL \compt_sortie[4]~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \s_compt_sortie[0]~5_combout\ : std_logic;
SIGNAL \rst~input_o\ : std_logic;
SIGNAL \rst~inputclkctrl_outclk\ : std_logic;
SIGNAL \s_compt_sortie[0]~6\ : std_logic;
SIGNAL \s_compt_sortie[1]~7_combout\ : std_logic;
SIGNAL \s_compt_sortie[1]~8\ : std_logic;
SIGNAL \s_compt_sortie[2]~9_combout\ : std_logic;
SIGNAL \s_compt_sortie[2]~10\ : std_logic;
SIGNAL \s_compt_sortie[3]~11_combout\ : std_logic;
SIGNAL \s_compt_sortie[3]~12\ : std_logic;
SIGNAL \s_compt_sortie[4]~13_combout\ : std_logic;
SIGNAL s_compt_sortie : std_logic_vector(4 DOWNTO 0);
SIGNAL \ALT_INV_rst~inputclkctrl_outclk\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk <= clk;
ww_rst <= rst;
compt_sortie <= ww_compt_sortie;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\rst~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \rst~input_o\);

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);
\ALT_INV_rst~inputclkctrl_outclk\ <= NOT \rst~inputclkctrl_outclk\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X0_Y55_N9
\compt_sortie[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_compt_sortie(0),
	devoe => ww_devoe,
	o => \compt_sortie[0]~output_o\);

-- Location: IOOBUF_X0_Y55_N23
\compt_sortie[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_compt_sortie(1),
	devoe => ww_devoe,
	o => \compt_sortie[1]~output_o\);

-- Location: IOOBUF_X0_Y54_N9
\compt_sortie[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_compt_sortie(2),
	devoe => ww_devoe,
	o => \compt_sortie[2]~output_o\);

-- Location: IOOBUF_X0_Y53_N9
\compt_sortie[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_compt_sortie(3),
	devoe => ww_devoe,
	o => \compt_sortie[3]~output_o\);

-- Location: IOOBUF_X0_Y55_N16
\compt_sortie[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_compt_sortie(4),
	devoe => ww_devoe,
	o => \compt_sortie[4]~output_o\);

-- Location: IOIBUF_X0_Y36_N8
\clk~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G2
\clk~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: LCCOMB_X1_Y55_N16
\s_compt_sortie[0]~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_compt_sortie[0]~5_combout\ = s_compt_sortie(0) $ (VCC)
-- \s_compt_sortie[0]~6\ = CARRY(s_compt_sortie(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => s_compt_sortie(0),
	datad => VCC,
	combout => \s_compt_sortie[0]~5_combout\,
	cout => \s_compt_sortie[0]~6\);

-- Location: IOIBUF_X0_Y36_N15
\rst~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rst,
	o => \rst~input_o\);

-- Location: CLKCTRL_G4
\rst~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \rst~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \rst~inputclkctrl_outclk\);

-- Location: FF_X1_Y55_N17
\s_compt_sortie[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \s_compt_sortie[0]~5_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_compt_sortie(0));

-- Location: LCCOMB_X1_Y55_N18
\s_compt_sortie[1]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_compt_sortie[1]~7_combout\ = (s_compt_sortie(1) & (!\s_compt_sortie[0]~6\)) # (!s_compt_sortie(1) & ((\s_compt_sortie[0]~6\) # (GND)))
-- \s_compt_sortie[1]~8\ = CARRY((!\s_compt_sortie[0]~6\) # (!s_compt_sortie(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => s_compt_sortie(1),
	datad => VCC,
	cin => \s_compt_sortie[0]~6\,
	combout => \s_compt_sortie[1]~7_combout\,
	cout => \s_compt_sortie[1]~8\);

-- Location: FF_X1_Y55_N19
\s_compt_sortie[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \s_compt_sortie[1]~7_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_compt_sortie(1));

-- Location: LCCOMB_X1_Y55_N20
\s_compt_sortie[2]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_compt_sortie[2]~9_combout\ = (s_compt_sortie(2) & (\s_compt_sortie[1]~8\ $ (GND))) # (!s_compt_sortie(2) & (!\s_compt_sortie[1]~8\ & VCC))
-- \s_compt_sortie[2]~10\ = CARRY((s_compt_sortie(2) & !\s_compt_sortie[1]~8\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => s_compt_sortie(2),
	datad => VCC,
	cin => \s_compt_sortie[1]~8\,
	combout => \s_compt_sortie[2]~9_combout\,
	cout => \s_compt_sortie[2]~10\);

-- Location: FF_X1_Y55_N21
\s_compt_sortie[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \s_compt_sortie[2]~9_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_compt_sortie(2));

-- Location: LCCOMB_X1_Y55_N22
\s_compt_sortie[3]~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_compt_sortie[3]~11_combout\ = (s_compt_sortie(3) & (!\s_compt_sortie[2]~10\)) # (!s_compt_sortie(3) & ((\s_compt_sortie[2]~10\) # (GND)))
-- \s_compt_sortie[3]~12\ = CARRY((!\s_compt_sortie[2]~10\) # (!s_compt_sortie(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => s_compt_sortie(3),
	datad => VCC,
	cin => \s_compt_sortie[2]~10\,
	combout => \s_compt_sortie[3]~11_combout\,
	cout => \s_compt_sortie[3]~12\);

-- Location: FF_X1_Y55_N23
\s_compt_sortie[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \s_compt_sortie[3]~11_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_compt_sortie(3));

-- Location: LCCOMB_X1_Y55_N24
\s_compt_sortie[4]~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_compt_sortie[4]~13_combout\ = \s_compt_sortie[3]~12\ $ (!s_compt_sortie(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => s_compt_sortie(4),
	cin => \s_compt_sortie[3]~12\,
	combout => \s_compt_sortie[4]~13_combout\);

-- Location: FF_X1_Y55_N25
\s_compt_sortie[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \s_compt_sortie[4]~13_combout\,
	clrn => \ALT_INV_rst~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_compt_sortie(4));

ww_compt_sortie(0) <= \compt_sortie[0]~output_o\;

ww_compt_sortie(1) <= \compt_sortie[1]~output_o\;

ww_compt_sortie(2) <= \compt_sortie[2]~output_o\;

ww_compt_sortie(3) <= \compt_sortie[3]~output_o\;

ww_compt_sortie(4) <= \compt_sortie[4]~output_o\;
END structure;


