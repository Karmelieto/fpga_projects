LIBRARY ieee;    
USE ieee.std_logic_1164.all;    
 
ENTITY TP5_reset_asynchrone IS PORT  
(ck : in std_logic;
SIGNAL s_out_compt : out STD_LOGIC_VECTOR(4 downto 0);
SIGNAL s_out : out STD_LOGIC);   
END TP5_reset_asynchrone;    
 
ARCHITECTURE pure_logic OF TP5_reset_asynchrone IS
 
component TP5_CompteurSimple
 PORT (
       clk, rst : IN STD_LOGIC;
       compt_sortie : out STD_LOGIC_VECTOR(4 DOWNTO 0)
);
END component;
 
component TP5_ComparateurSimple
	port (a : in std_logic_vector(4 downto 0);
	equals : out std_logic);
end component;

 
signal rst : std_logic; 
 
signal out_compteur : STD_LOGIC_VECTOR(4 DOWNTO 0);
 
BEGIN
 
c1 : TP5_CompteurSimple port map(ck, rst, out_compteur);
 
c2 : TP5_ComparateurSimple port map(out_compteur, s_out);
 
 
END pure_logic;
