library IEEE;
use IEEE.std_logic_1164.all;

entity TP5_ComparateurSimple is
    port (a: in std_logic_vector(4 downto 0);
    equals : out std_logic);
end TP5_ComparateurSimple;

architecture dataflow of TP5_ComparateurSimple is
begin
    equals <= '1' when (a =  "11111") else '0';
end dataflow;