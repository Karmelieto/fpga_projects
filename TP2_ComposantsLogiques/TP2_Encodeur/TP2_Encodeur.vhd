LIBRARY ieee;
use ieee.std_logic_1164.all;
ENTITY TP2_Encodeur IS
PORT ( a : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		 s : OUT STD_LOGIC_VECTOR(1 DOWNTO 0));
END TP2_ENCODEUR;

Architecture archi of TP2_Encodeur IS
BEGIN
s <= "11" when a="1000" else
		"10" when a="0100" else
		"01" when a="0010" else
		"00" when a="0001";
END archi;