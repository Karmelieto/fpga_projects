library IEEE;
use IEEE.STD_LOGIC_1164.all;
 
entity TP2_Demultiplexeur is
 port(
 D : in STD_LOGIC;
 A : in STD_LOGIC_VECTOR(1 DOWNTO 0);
 Q : out STD_LOGIC_VECTOR(3 DOWNTO 0)
 );
end TP2_Demultiplexeur;
 
architecture archi of TP2_Demultiplexeur is
begin
	process (D,A) is
	begin
	if D='1' then
		 if A="00" then 
			Q <= "0001";
		 elsif A="01" then
			Q <="0010";
		 elsif A="10" then
			Q <="0100";
		 elsif A="11" then
			Q <="1000";
		 end if;
	else Q <="0000";
		 end if;
	end process;
end archi;