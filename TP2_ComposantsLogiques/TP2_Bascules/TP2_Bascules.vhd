library IEEE;
use ieee.std_logic_1164.all;

entity TP2_Bascules is port (
clk : in std_logic;
d : in std_logic;
q : out std_logic);
end TP2_Bascules;

architecture archi of TP2_Bascules IS
BEGIN
	process (clk)
	BEGIN
		if rising_edge (clk) then
			q<=d;
			end if;
		end process;
end archi; 