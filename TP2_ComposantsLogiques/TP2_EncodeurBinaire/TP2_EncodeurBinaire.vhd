Library ieee;

Use ieee.std_logic_1164.all;

entity TP2_EncodeurBinaire is
    Port ( input : in STD_LOGIC_VECTOR (3 downto 0);
           output : out STD_LOGIC_VECTOR (2 downto 0));
end TP2_EncodeurBinaire;

architecture Behavior of TP2_EncodeurBinaire is

begin
   process(input)
   begin
      if (input(3)='1') then
         output <= "111";
      elsif (input(2)='1') then
         output <= "101";
      elsif (input(1)='1') then
         output <= "011";
      elsif (input(0)='1') then
         output <= "001";
      else
         output <= "000";
   end if;
end process;

end Behavior;