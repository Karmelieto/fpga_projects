-- Copyright (C) 2019  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 19.1.0 Build 670 09/22/2019 SJ Standard Edition"

-- DATE "05/05/2020 14:40:54"

-- 
-- Device: Altera EP4CE115F29I8L Package FBGA780
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_N7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCEO~	=>  Location: PIN_P28,	 I/O Standard: 2.5 V,	 Current Strength: 8mA


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	TP2_Compteur_Processus IS
    PORT (
	clk : IN std_logic;
	compt_sortie : BUFFER std_logic_vector(3 DOWNTO 0);
	horloge_sortie : BUFFER std_logic
	);
END TP2_Compteur_Processus;

-- Design Ports Information
-- compt_sortie[0]	=>  Location: PIN_T3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- compt_sortie[1]	=>  Location: PIN_R4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- compt_sortie[2]	=>  Location: PIN_R5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- compt_sortie[3]	=>  Location: PIN_T7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- horloge_sortie	=>  Location: PIN_T4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF TP2_Compteur_Processus IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_compt_sortie : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_horloge_sortie : std_logic;
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \compt_sortie[0]~output_o\ : std_logic;
SIGNAL \compt_sortie[1]~output_o\ : std_logic;
SIGNAL \compt_sortie[2]~output_o\ : std_logic;
SIGNAL \compt_sortie[3]~output_o\ : std_logic;
SIGNAL \horloge_sortie~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \s_compt_sortie[0]~3_combout\ : std_logic;
SIGNAL \s_compt_sortie[1]~0_combout\ : std_logic;
SIGNAL \s_compt_sortie[2]~1_combout\ : std_logic;
SIGNAL \s_compt_sortie[3]~2_combout\ : std_logic;
SIGNAL \LessThan1~0_combout\ : std_logic;
SIGNAL s_compt_sortie : std_logic_vector(3 DOWNTO 0);
SIGNAL \ALT_INV_LessThan1~0_combout\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk <= clk;
compt_sortie <= ww_compt_sortie;
horloge_sortie <= ww_horloge_sortie;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);
\ALT_INV_LessThan1~0_combout\ <= NOT \LessThan1~0_combout\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X0_Y32_N16
\compt_sortie[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_compt_sortie(0),
	devoe => ww_devoe,
	o => \compt_sortie[0]~output_o\);

-- Location: IOOBUF_X0_Y33_N16
\compt_sortie[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_compt_sortie(1),
	devoe => ww_devoe,
	o => \compt_sortie[1]~output_o\);

-- Location: IOOBUF_X0_Y32_N23
\compt_sortie[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_compt_sortie(2),
	devoe => ww_devoe,
	o => \compt_sortie[2]~output_o\);

-- Location: IOOBUF_X0_Y31_N16
\compt_sortie[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => s_compt_sortie(3),
	devoe => ww_devoe,
	o => \compt_sortie[3]~output_o\);

-- Location: IOOBUF_X0_Y33_N23
\horloge_sortie~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_LessThan1~0_combout\,
	devoe => ww_devoe,
	o => \horloge_sortie~output_o\);

-- Location: IOIBUF_X0_Y36_N8
\clk~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G2
\clk~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: LCCOMB_X1_Y32_N12
\s_compt_sortie[0]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_compt_sortie[0]~3_combout\ = !s_compt_sortie(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => s_compt_sortie(0),
	combout => \s_compt_sortie[0]~3_combout\);

-- Location: FF_X1_Y32_N13
\s_compt_sortie[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \s_compt_sortie[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_compt_sortie(0));

-- Location: LCCOMB_X1_Y32_N2
\s_compt_sortie[1]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_compt_sortie[1]~0_combout\ = s_compt_sortie(1) $ (s_compt_sortie(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => s_compt_sortie(1),
	datad => s_compt_sortie(0),
	combout => \s_compt_sortie[1]~0_combout\);

-- Location: FF_X1_Y32_N3
\s_compt_sortie[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \s_compt_sortie[1]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_compt_sortie(1));

-- Location: LCCOMB_X1_Y32_N28
\s_compt_sortie[2]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_compt_sortie[2]~1_combout\ = s_compt_sortie(2) $ (((s_compt_sortie(1) & s_compt_sortie(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => s_compt_sortie(1),
	datac => s_compt_sortie(2),
	datad => s_compt_sortie(0),
	combout => \s_compt_sortie[2]~1_combout\);

-- Location: FF_X1_Y32_N29
\s_compt_sortie[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \s_compt_sortie[2]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_compt_sortie(2));

-- Location: LCCOMB_X1_Y32_N30
\s_compt_sortie[3]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \s_compt_sortie[3]~2_combout\ = s_compt_sortie(3) $ (((s_compt_sortie(0) & (s_compt_sortie(2) & s_compt_sortie(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => s_compt_sortie(0),
	datab => s_compt_sortie(2),
	datac => s_compt_sortie(3),
	datad => s_compt_sortie(1),
	combout => \s_compt_sortie[3]~2_combout\);

-- Location: FF_X1_Y32_N31
\s_compt_sortie[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \s_compt_sortie[3]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_compt_sortie(3));

-- Location: LCCOMB_X1_Y32_N24
\LessThan1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \LessThan1~0_combout\ = (!s_compt_sortie(3) & (((!s_compt_sortie(0)) # (!s_compt_sortie(2))) # (!s_compt_sortie(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => s_compt_sortie(1),
	datab => s_compt_sortie(2),
	datac => s_compt_sortie(3),
	datad => s_compt_sortie(0),
	combout => \LessThan1~0_combout\);

ww_compt_sortie(0) <= \compt_sortie[0]~output_o\;

ww_compt_sortie(1) <= \compt_sortie[1]~output_o\;

ww_compt_sortie(2) <= \compt_sortie[2]~output_o\;

ww_compt_sortie(3) <= \compt_sortie[3]~output_o\;

ww_horloge_sortie <= \horloge_sortie~output_o\;
END structure;


