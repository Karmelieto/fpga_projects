LIBRARY ieee;    
USE ieee.std_logic_1164.all;    

ENTITY TP2_Compteur IS PORT  
(Ck_CPT, J_CPT, K_CPT : in std_logic;  
q1, q2, q3, q4 : out std_logic);    
END TP2_Compteur;    
  

ARCHITECTURE pure_logic OF TP2_Compteur IS    

component BasculeJK  
Port (Ck, J, K : in std_logic;  
        Q, nonQ : out std_logic); 
end component; 
signal m1, m2, m3, m4 : std_logic; 


BEGIN    

c1 : BasculeJK port map(Ck_CPT,'1','1',q1,m1); 

c2 : BasculeJK port map(m1,'1','1',q2,m2); 

c3 : BasculeJK port map(m2,'1','1',q3,m3); 

c4 : BasculeJK port map(m3,'1','1',q4,m4);

END pure_logic; 