Library ieee;  
Use ieee.std_logic_1164.all;  


ENTITY BasculeJK IS 
Port (Ck, J, K : in std_logic;  
        Q, nonQ : out std_logic); 
END BasculeJK;  

ARCHITECTURE STRUCTURAL OF BasculeJK IS 

Signal Qa  : std_logic;  

BEGIN 

PROCESS(Ck,J,K) 

BEGIN 
    If Ck'event and CK='1'    then 
        IF J='0' and K ='0' then     Qa <= Qa; 
        ELSIF J='0' and K='1' then Qa <= '0';  
        ELSIF J='1' and K='0'    then Qa <= '1';  
        ELSE     Qa <= NOT Qa;  
        END IF;  
    END IF; 
    END PROCESS;  
    Q <= Qa; 
	 nonQ <= NOT Qa;
END STRUCTURAL; 