LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;


ENTITY TP2_Compteur_Processus IS
 PORT (
       clk : IN STD_LOGIC;
       compt_sortie : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
       horloge_sortie : OUT STD_LOGIC
		);
END TP2_Compteur_Processus;

ARCHITECTURE rtl OF TP2_Compteur_Processus IS 
	SIGNAL s_compt_sortie : STD_LOGIC_VECTOR(3 DOWNTO 0);
BEGIN
PROCESS(clk)
BEGIN
 IF clk'EVENT AND clk = '1' THEN
   IF s_compt_sortie >= 15 THEN
         s_compt_sortie <= "0000";
   ELSE
         s_compt_sortie <= s_compt_sortie + 1;
   END IF;
 END IF;
END PROCESS;
	compt_sortie <= s_compt_sortie;
	horloge_sortie <= '0' WHEN s_compt_sortie < 7
		else
	'1';
END;
